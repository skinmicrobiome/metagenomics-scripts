#!/bin/env perl

use strict;
use warnings;

my @msh = split(/ /,<>);
chomp @msh;

print STDERR "\n"; $|++;
my $cnt = 0;
while ( scalar(@msh) >= 2) {
    printf STDERR "\r\r\r\r\r\r%6s",++$cnt;
    system("cat @msh | tr '\n' '\0' | xargs -0 /home/fbreitwieser/bin/mash dist") == 0 
        or die "\n\nsystem 'cat [".scalar(@msh)." seq] | xargs -0 mash dist' failed: code $?; $!";
    shift @msh;
}
