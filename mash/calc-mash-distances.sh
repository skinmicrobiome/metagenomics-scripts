#!/bin/bash

## Calculate all pairwise distances between mash files

mkdir -p dist

while [ "${2+defined}" ]; do
        >&2 echo $1
        DIST_F=${1/sketches/dist}-dist.tab
        mkdir -p `dirname $DIST_F`
        mash dist $*  > $DIST_F
        shift
done
