#!/usr/bin/perl

# Copyright 2013-2015, Derrick Wood <dwood@cs.jhu.edu>
#
# This file is part of the Kraken taxonomic sequence classification system.
#
# Kraken is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Kraken is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Kraken.  If not, see <http://www.gnu.org/licenses/>.

# Reports a summary of Kraken's results.

use strict;
use warnings;
use File::Basename;
use Getopt::Long;
use SuffixTree;
use Storable;
use FindBin qw/$Bin/;

my $PROG = basename $0;
my $KRAKEN_DIR = "/data1/igm3/sw/packages/kraken-0.10.5-beta/install";

# Test to see if the executables got moved, try to recover if we can
if (! -e "$KRAKEN_DIR/classify") {
  use Cwd 'abs_path';
  $KRAKEN_DIR = dirname abs_path($0);
}

require "$KRAKEN_DIR/krakenlib.pm";

my $show_zeros = 0;
my $db_prefix;

GetOptions(
  "help" => \&display_help,
  "version" => \&display_version,
  "show-zeros" => \$show_zeros,
  "db=s" => \$db_prefix,
) || usage();
usage() unless defined $db_prefix;

#$is_fasta = ($ARGV[1] =~ /.fa(?:sta)?(?:.gz)?/) ? 1 : 0;

die unless -d "$db_prefix/taxonomy" ;

eval { $db_prefix = krakenlib::find_db($db_prefix); };
if ($@) {
  die "$PROG: $@";
}

sub usage {
  my $exit_code = @_ ? shift : 64;
  print STDERR "
Usage
  $PROG [OPTIONS] --db=DATABASE <kraken output file> 
  
Options
  --help
  --version
  --show-zeros
  --db=DATABASE
    ";
  my $default_db;
  eval { $default_db = krakenlib::find_db(); };
  if (defined $default_db) {
    print STDERR "\n   Default database is \"$default_db\"\n";
  }
  exit $exit_code;
}

sub display_help {
  usage(0);
}

sub display_version {
  print "Kraken version 0.10.5-beta\n";
  print "Copyright 2013-2015, Derrick Wood (dwood\@cs.jhu.edu)\n";
  print "modified by fbreitwieser to count k-mers\n";
  exit 0;
}

print STDERR "Loading taxonomy ... ";
load_taxonomy($db_prefix);
print STDERR " done\n";

my ($child_lists, $name_map, $rank_map);

my (%taxo_counts,%kmer_counts,%taxon_to_kmer,%taxon,%taxon_to_substr);
my $seq_count = 0;
$taxo_counts{0} = 0;

## DB kmer-counts
print STDERR "Loading kmercounts ... ";
#my $taxid_kmercount_f = "$db_prefix/taxid_kmercounts.csv";
#if (!-f $taxid_kmercount_f) {
#    print STDERR "Counting kmers ... (will take several minutes)\n";
#    system("$Bin/count_kmers.pl | tee $db_prefix/taxid_kmercounts.csv") == 0 or die "Running count_kmers failed: $!";
#}

my $taxid_kmercount_f = shift;

open my $TK, '<', $taxid_kmercount_f or die "Error opening file $taxid_kmercount_f: $!";
while (<$TK>) {
    chomp;
    my ($taxid, $cnt) = split(/\t/);
    $taxo_counts{$taxid} = $cnt;
    $seq_count += $cnt;
}
print STDERR " done, loaded ".(scalar keys %taxo_counts)."\n";

my %clade_counts = %taxo_counts;
dfs_summation(1);

for (keys %$name_map) {
  $clade_counts{$_} ||= 0;
}

print join("\t",qw/perc n-clade n-stay level taxonid depth name/);
print "\n";

dfs_report(1, 0, 0);

sub dfs_report {
  my ($node, $depth, $depth2 ) = @_;
  if (! $clade_counts{$node} && ! $show_zeros) {
    return;
  }
  $kmer_counts{$node} = 0 unless defined $kmer_counts{$node};
  my $rank_code = rank_code($rank_map->{$node});
  ++$depth2 if $rank_code ne "-";

  printf "%6.2f\t%d\t%d\t%s\t%d\t%d\t%s%s\n",
    ($clade_counts{$node} || 0) * 100 / $seq_count,
    ($clade_counts{$node} || 0),
    ($taxo_counts{$node} || 0),
    $rank_code,
    $node,
    $depth,
    "  " x $depth,
    $name_map->{$node};


  my $children = $child_lists->{$node};
  if ($children) {
    my @sorted_children = sort { $clade_counts{$b} <=> $clade_counts{$a} } @$children;
    for my $child (@sorted_children) {
      dfs_report($child, $depth + 1, $depth2);
    }
  }
}

sub rank_code {
  my $rank = shift;
  for ($rank) {
    $_ eq "species" and return "S";
    $_ eq "genus" and return "G";
    $_ eq "family" and return "F";
    $_ eq "order" and return "O";
    $_ eq "class" and return "C";
    $_ eq "phylum" and return "P";
    $_ eq "kingdom" and return "K";
    $_ eq "superkingdom" and return "D";
  }
  return "-";
}

sub dfs_summation {
  my $node = shift;
  my $children = $child_lists->{$node};
  if ($children) {
    for my $child (@$children) {
      dfs_summation($child);
      $clade_counts{$node} += ($clade_counts{$child} || 0);
    }
  }
}

sub load_taxonomy {
  my $prefix = shift;
  my $node_map;

  if (-f "$prefix/taxonomy/name_map.perlhash") {
    $name_map = retrieve("$prefix/taxonomy/name_map.perlhash");
  } else {
    open NAMES, "<", "$prefix/taxonomy/names.dmp"
      or die "$PROG: can't open names file: $!\n";
    while (<NAMES>) {
      chomp;
      s/\t\|$//;
      my @fields = split /\t\|\t/;
      my ($node_id, $name, $type) = @fields[0,1,3];
      if ($type eq "scientific name") {
        $name_map->{$node_id} = $name;
        $node_map->{$name} = $node_id unless defined $node_map->{$name};
      }
    }
    close NAMES;
    store($name_map,"$prefix/taxonomy/name_map.perlhash");
    store($node_map,"$prefix/taxonomy/node_map.perlhash");
  }

  if (-f "$prefix/taxonomy/child_lists.perlhash" && -f "$prefix/taxonomy/rank_map.perlhash") {
    $child_lists = retrieve("$prefix/taxonomy/child_lists.perlhash");
    $rank_map = retrieve("$prefix/taxonomy/rank_map.perlhash");
  } else {
    open NODES, "<", "$prefix/taxonomy/nodes.dmp"
      or die "$PROG: can't open nodes file: $!\n";
    while (<NODES>) {
      chomp;
      my @fields = split /\t\|\t/;
      my ($node_id, $parent_id, $rank) = @fields[0,1,2];
      if ($node_id == 1) {
        $parent_id = 0;
      }
      $child_lists->{$parent_id} ||= [];
      push @{ $child_lists->{$parent_id} }, $node_id;
      $rank_map->{$node_id} = $rank;
    }
    close NODES;

    store($child_lists, "$prefix/taxonomy/child_lists.perlhash");
    store($rank_map, "$prefix/taxonomy/rank_map.perlhash");
  }
}
