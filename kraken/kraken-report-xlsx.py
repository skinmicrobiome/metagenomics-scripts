#!/bin/env python

import os
import glob
import pandas
import sys
from xlsxwriter.workbook import Workbook

if len(sys.argv) <= 1:
    script_name = os.path.basename(sys.argv[0])
    print script_name+": Write XLSX version of Kraken report file"
    print "Usage:   "+script_name+" KRAKEN_REPORT\n"
    exit(1)

is_kraken_report = True
if is_kraken_report:
    summed_columns = "($B$2 + $B$3)"
else:
    summed_columns = "$B$2"

csvfile = sys.argv[1]
level_column = 3
depth_column = 5
name_column = 6

colors = ["#b3e2cd", "#fdcdac", "#cbd5e8", "#f4cae4", "#e6f5c9", "#fff2ae", "#f1e2cc", "#cccccc"]
color_idx = -1

# Create workbook and worksheet
workbook = Workbook(csvfile + '.xlsx',  {'constant_memory': True, 'nan_inf_to_errors': True})
worksheet = workbook.add_worksheet()
worksheet.outline_settings(True, False,False)

# Add a bold, percent, bgcolors format
bold_format = workbook.add_format({'bold': 1})
percent_format = workbook.add_format({'num_format': '0.00%'})  
number_format = workbook.add_format({'num_format': '#,##0'})

color_formats = []
bold_color_formats = []
for i, col in enumerate(colors):
    color_formats.append(workbook.add_format({'bg_color': col}))
    bold_color_formats.append(workbook.add_format({'bg_color': col, 'bold': True}))

# Read input file into a Panda DataFrame
with open(csvfile, 'rb') as f:
    df = pandas.read_csv(f, sep='\t', header=0)

# Write header
for c, col in enumerate(df.columns):
    if c <= 2:
        worksheet.write(0, c, col, bold_format)
    if c == 3:
        worksheet.write(0, c, "perc-domain", bold_format)
        worksheet.write(0, c+1, "perc-domain-stay", bold_format)
        # level column
        worksheet.write(0, c+2, col, bold_format)
    else:
        worksheet.write(0, c+2, col, bold_format)



worksheet.autofilter(0,0,0,len(df.columns)-1)

# Write data
nrow = len(df.index)
curr_depth = -1
start_depth = 0
same_depth_as_before = True
domain_reads = 0

for index, row in df.iterrows():
    # for grouping
    my_format = None
    if df.level[index] == 'D':
        curr_depth = 0
        start_depth = df.depth[index]
        domain_reads = df.get('n-clade')[index]
        color_idx += 1
        my_format = bold_color_formats[color_idx]
    elif curr_depth >= 0:
        depth_before = curr_depth
        curr_depth = min(df.depth[index] - start_depth, 7)
        if  curr_depth <= 0:
            color_idx += 1
            domain_reads = df.get('n-clade')[index]
            start_depth = df.depth[index]
        same_depth_as_before = curr_depth == depth_before

    if curr_depth > 0:
        worksheet.set_row(index + 1, None, None, {'level': curr_depth, 'hidden': True, 'collapsed': True})
        my_format = color_formats[color_idx]

    for c, col in enumerate(row):
        if c == 0:
            # in the first column, calculate the percentage of reads at that level
            worksheet.write(index + 1, c, "=B"+str(index+2)+"/"+summed_columns, percent_format)
        elif c  <= 2:
            worksheet.write(index + 1, c, col, number_format)
        elif c == 3:
            # insert percent-domain column
            if domain_reads > 0:
                worksheet.write(index + 1, c, "=B"+str(index+2)+"/"+str(domain_reads), percent_format)
                worksheet.write(index + 1, c+1, "=C"+str(index+2)+"/"+str(domain_reads), percent_format)

            #level column
            worksheet.write(index + 1, c+2, col)
        elif c == name_column:
            worksheet.write(index + 1, c+2, col, my_format)
        else:
            worksheet.write(index + 1, c+2, col)

# Add conditional formatting
worksheet.conditional_format(1,0,nrow+1,0,{'type': 'data_bar'})
worksheet.conditional_format(1,3,nrow+1,3,{'type': 'data_bar', 'bar_color': 'orange' })
worksheet.conditional_format(1,4,nrow+1,4,{'type': 'data_bar', 'bar_color': 'yellow' })

workbook.close()


