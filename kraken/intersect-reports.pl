#!/bin/env perl

## Take two reports, and report only clades that are in both of them with a minimum number of reads

use strict;
use warnings;
use File::Basename;

if (scalar @ARGV != 3) {
  print STDERR "
USAGE: ".basename($0)." <min number of reads> <kraken report A> <kraken report B>

Reports only clades that are in both report A and B with a minimum number of reads.

";
  exit 1;
}

my ($min_reads,$file1,$file2) = @ARGV;

my %f1;
open (my $F1,"<",$file1) or die "Couldn't open $file1: $!";
while (<$F1>) {
    my @l = split(/\t/);
    next unless $l[1] >= $min_reads;
    $f1{$l[4]} = \@l;
}
close $F1;

my %f2;
open (my $F2,"<",$file2) or die "Couldn't open $file2: $!";
while (<$F2>) {
    my @l = split(/\t/);
    next unless $l[1] >= $min_reads;
    if (defined ($f1{$l[4]})) {
        print join("\t",@{$f1{$l[4]}});
        print $_;
    }
}
close $F2;



