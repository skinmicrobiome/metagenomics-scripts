#!/bin/env perl

use strict;
use warnings;

## Usage: $0 database.kdb > taxid_kmercounts.csv

my $jellyfish = "jellyfish1";
my $db = defined $ARGV[0] ? $ARGV[0] : "database.kdb";
my %taxid_kmercount;

open (my $DB, "jellyfish1 dump $db |") or die "Error opening $db: $!";
while (<$DB>) {
    $taxid_kmercount{$_} += 1;
    <$DB>;
}

foreach my $taxid (sort keys %taxid_kmercount) {
    my $count = $taxid_kmercount{$taxid};
    chomp $taxid;
    $taxid =~ s/^>//;
    printf "%s\t%s\n", $taxid, $count;
}
