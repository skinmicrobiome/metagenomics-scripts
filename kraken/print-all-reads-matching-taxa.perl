#!/bin/env perl
# take a taxon id and a fastq files with reads.  Find all the read IDs in a kraken file and then
# go through the fastq file and print all those reads
#  $taxon_id = 11676;  this is for HIV
#  $taxon_id = 10632;  
# USE OPTION -f to filter fasta file

use strict;
use warnings;
use Getopt::Std;
use File::Basename;

my %print_it;
my $KRAKEN;

my $usage = "
".basename($0).": Extract all reads from FASTQ file that are matched to a specied taxon by Kraken

Usage: ".basename($0)." [OPTIONS] <taxon> <kraken> <fasta/fastq>

<taxon>         taxonomy ID, possibly multiple separated by ','
<kraken>        kraken result file
<fasta/fastq>   fasta/fastq file, possibly gzipped

Options:
  -a  input is FASTA file (default: FASTQ)
  -f  output in FASTA format
  -i  invert: print all reads not matching taxon
  -v  verbose
  -p  paired-end reads: use a '%' in fasta/q file name as placeholder for 1 and 2

Example:
    ".basename($0)." -p 9606 result.kraken input_%.fq 
    outputs all reads of input_1.fq and input_2.fq that have the taxonomy ID 9606 to STDOUT.
";

my %options=(
    i=>0
);
getopts("fviap", \%options) || die $usage;
my $is_paired = $options{p};
my $is_inverted = $options{i};
$is_inverted=0 unless defined $is_inverted;

die "$usage" unless $#ARGV == 2;

my %taxon_id = map {$_ => 0} split(/,/,$ARGV[0]);
print STDERR "Getting all reads ".
            ($is_inverted ? "not matched to" : "matched to")." the following taxon: ",join(", ",(keys %taxon_id)),"\n";
#%taxon_id = () if $is_inverted;

if ($ARGV[1] =~ /.gz$/) {
    open $KRAKEN, "gunzip -c $ARGV[1] |" or die $!;
} else {
    open $KRAKEN, '<', $ARGV[1] or die $!;
}



## open fastq_files
my ($FQ,$FQ_2);
if ($ARGV[2] =~ /.gz$/) {
    if ($is_paired) {
        open $FQ, "gunzip -c ".get_p($ARGV[2],1)." |" or die $!;
        open $FQ_2, "gunzip -c ".get_p($ARGV[2],2)." |" or die $!;
    } else {
        open $FQ, "gunzip -c $ARGV[2] |" or die $!;
    }
} else {
    if ($is_paired) {
        open $FQ, '<', get_p($ARGV[2],1) or die $!;
        open $FQ_2, '<', get_p($ARGV[2],2) or die $!;
    } else {
        open $FQ, '<', $ARGV[2] or die $!;
    }
}

while (<$KRAKEN>) {
    # kraken file format is:
    # C<tab>M02302:53:000000000-ACJM9:1:1101:14778:1726<tab>9606<tab>151<tab>9606:56 0:55 9606:2 0:8
    my ($tmp1, $readid, $taxid, $len, $kmer_hits) = split("\t");
    #print STDERR '($tmp1, $readid, $taxid, $len, $kmer_hits) = '."($tmp1, $readid, $taxid, $len, $kmer_hits)\n";
    if (!$is_inverted && defined $taxon_id{$taxid}) {
        $print_it{$readid} = $taxid;  # mark it for printing
        $taxon_id{$taxid} += 1;
    } elsif ($is_inverted && !defined $taxon_id{$taxid}) {
        $print_it{$readid} = 1;  # mark it for printing
        $taxon_id{"!$ARGV[0]"} += 1;
    }
}

my $sum_reads = 0;
for my $taxon (keys %taxon_id) {
    printf STDERR "  Got %5s reads for %s\n",$taxon_id{$taxon},$taxon;
    $sum_reads += $taxon_id{$taxon};
}
exit(0) if $sum_reads==0;

$options{f} = 1 if (defined $options{a});

my $cnt=0;
$\ = undef ;
print STDERR "Going through FA/FQ file to get reads";
my $previous;

my $start_symbol = defined $options{a}? '>' : '@';

while(<$FQ>) {

    next unless (/^$start_symbol/);   # if not at first line of fastq entry
    my ($id, $other) = split(" ");

    $id =~ s/^$start_symbol//; # get rid of ">" character
    $id =~ s/ .*//;            # remove everything after the space
    if ($is_paired) {
        # remove pair-specific part of read IDs (if present)
        $id =~ s#/[12]$##;  # || die "$id??";
    }
    $id =~ s#/[12]$##; ## paired read ends

    my $print_this_read = defined $print_it{$id};

    if ($print_this_read) { # print all 4 lines
        if (defined $options{f}) {
            die if $is_paired;  ##NOT IMPLEMENTED YET
            ## output in FASTA format
            #print ">",$id," matches ",$print_it{$id},"\n";
            print ">",$id,"\n";
            my $next = <$FQ>;
            print $next;
 
            if (defined $options{a}) {
              while ($next = <$FQ> && !$next =~ /^>/ ) {
                print $next;
              }
              $_ = $next;
              redo; # redo the with $_ set to $next
            } else {
              <$FQ>;
              <$FQ>;
            }
        } else {
            ## output in FASTQ format
            print $_;
            for (my $i = 0; $i<3; $i++) {
                my $next = <$FQ>;
                print $next;
            }
            if ($is_paired) {
                for (my $i = 0; $i<4; $i++) {
                    my $next = <$FQ_2>;
                    print $next;
                }
            }
        }

        ++$cnt;
        if ($cnt % 100 eq 0) {
            printf STDERR "\rnumber of extracted reads: %10s",$cnt; 
            $|++;
        }
        last if $cnt >= $sum_reads;
    } else {
        if (!defined $options{a}) {
            # skip next 3 fastq lines
            for (my $i = 0; $i<3; $i++) { <$FQ>; }
            # skip next 4 fastq lines in mate-pair (when paired) - header was not read
            if ($is_paired) { 
                for (my $i = 0; $i<4; $i++) { <$FQ_2>; } 
            }

        }
    }
}
printf STDERR "\rnumber of extracted reads: %10s",$cnt; 
print STDERR "\n\n";

close $KRAKEN;
close $FQ;

sub get_p {
    my ($s,$p) = @_;
    $s =~ s/%/$p/;
    return $s;
}


