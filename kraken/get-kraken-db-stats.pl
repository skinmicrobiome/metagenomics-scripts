#!/bin/env perl

use strict;
use warnings;
use Storable;

my $prefix = $ARGV[0];
$prefix = "." unless defined $prefix;

my @db_taxids=`cut -f 1 $prefix/taxid_kmercounts.csv`;
my @db_taxcnts=`cut -f 2 $prefix/taxid_kmercounts.csv`;
chomp @db_taxids;
chomp @db_taxcnts;
my %db_taxids;
for (my$i=0; $i <= $#db_taxids; ++$i) { $db_taxids{$db_taxids[$i]} = $db_taxcnts[$i]; }

my $name_map = retrieve("$prefix/taxonomy/name_map.perlhash");
my $node_map = retrieve("$prefix/taxonomy/node_map.perlhash");
my $child_lists = retrieve("$prefix/taxonomy/child_lists.perlhash");
my $rank_map = retrieve("$prefix/taxonomy/rank_map.perlhash");


my %stop_here = (
    Viruses=>1,
    Viroids=>1,
    Bacteria=>1,
    Fungi=>1,
    Metazoa=>1,
    "unclassified sequences"=>1,
    "other sequences"=>1,
    Archaea=>1,
    Rhodophyta=>1,
    Heterolobosea=>1,
    Viridiplantae=>1,
    Alveolata=>1,
    Euglenozoa=>1,
    Amoebozoa=>1
);



my @cnt_them = ("Bacteria","Fungi","Viruses");

get_dwn_cnt(1,0);

#for my $domain (@cnt_them) {
#    print "$domain:    ";
#    print_cnt($node_map->{$domain});
#}

sub print_cnt {
    my ($node, $depth) = @_;
    my $name = $name_map->{$node};
    my ($result,$kmer_cnt) = report_cnt($node,0);

    return 0 unless $result > 0;

    my $species_cnt = $result >> 32;
    my $mask = 2**32 - 1;
    my $subspecies_cnt = $result & $mask;
    print( ("-"x$depth)."$name: $species_cnt; $subspecies_cnt; $kmer_cnt\n");

    return 1;

}

sub get_dwn_cnt {
    my ($node, $depth) = @_;
    return if ($depth > 10);
    if ($depth == 0 || $rank_map->{$node} =~ /^[a-z]/) {
        my $res = print_cnt($node,$depth);
        return unless $res;
    }
    #print $name_map->{$node}.": ".(defined $stop_here{$name_map->{$node}})."\n";
    if (defined $stop_here{$name_map->{$node}}) {
        return;
    }
    #print "weiter\n";

    $depth += 1;
	my $children = $child_lists->{$node};
	if ($children) {
	    for my $child (@$children) {
		    get_dwn_cnt($child,$depth);
	    }
	}
}


sub report_cnt {
my ($node,$get_subspecies) = @_;

    my $kmer_cnt = defined $db_taxids{$node}? $db_taxids{$node} : 0;
	my $species_cnt = 0;
	if ($rank_map->{$node} eq 'species') {
        if (defined $db_taxids{$node}) {
    	    $species_cnt = 1 << 32 ;
        }
        $get_subspecies = 1;
	} #elsif ($get_subspecies) {
    #    $species_cnt = 1 if defined $db_taxids{$node};
    #}
	my $children = $child_lists->{$node};

	if ($children) {
	    for my $child (@$children) {
		    my ($a_species_cnt,$a_kmer_cnt) = report_cnt($child,$get_subspecies);
            $species_cnt += $a_species_cnt;
            $kmer_cnt += $a_kmer_cnt;
	    }
	} else {
        $species_cnt += 1 if defined $db_taxids{$node};
    }
    return ($species_cnt,$kmer_cnt);
}
