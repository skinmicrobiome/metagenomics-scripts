#include "taxdb.h"

#include <iostream>
#include <getopt.h>
#include <set>
#include <unordered_map>

#define no_argument 0
#define required_argument 1 
#define optional_argument 2

using namespace std;

size_t read_column = 0;
size_t taxid_column = 8;
size_t score_column = 3;
size_t ac_column = 1;

int main(int argc, char **argv) {

  string taxDB_file = argv[1];
  bool do_ac_mapping = false;
  const struct option longopts[] =
  {
  //  {"help",      no_argument,        0, 'h'},
    {"ac-column", required_argument, 0, 'a'},
    {"taxid-column", required_argument, 0, 't'},
    {"ac2taxid", required_argument, 0, 'm'},
    {0,0,0,0},
  };

  string ac2taxid_fileName;

  int index = 0;
  int iarg=0;
  //turn off getopt error message
  opterr=1; 
  while(iarg != -1) {
    iarg = getopt_long(argc, argv, "t:m:", longopts, &index);

    switch (iarg) {
      case 't': taxid_column = stoi(optarg); break;
      case 'm': ac2taxid_fileName = optarg; do_ac_mapping = true; break;
    }
  }

  unordered_map<string, uint32_t> ac2taxid;
  if (!ac2taxid_fileName.empty()) {
    std::ifstream ac2taxidFile(ac2taxid_fileName);
	string ac;
	uint32_t taxid;
    if (!ac2taxidFile.is_open())
      throw std::runtime_error("unable to open ac2taxiduence file");
	while (ac2taxidFile >> ac >> taxid) {
      ac2taxid[ac] = taxid;
    }
	cerr << "Read ac2taxid file" << endl;
  }
  

  TaxonomyDB<uint32_t, uint64_t> taxDB(taxDB_file);
  unordered_map<uint32_t, uint32_t> Parent_map = taxDB.getParentMap();

  std::cerr << "Reading file. Read col: " << read_column << "; taxid col: " << taxid_column << endl;
  std::string line;
  string previous_read = "bla";
  string current_read;
  uint32_t lca_taxid = 0;
  uint32_t current_taxid = 0;
  string current_ac;
  while (std::getline(std::cin, line)) {
    istringstream ss(line);
    size_t i = 0;
    for (; i < read_column; ++i) { ss.ignore(256,'\t'); }
    ss >> current_read;
  string taxDB_file = argv[1];
	if (do_ac_mapping) {
    	for (; i < ac_column; ++i) { ss.ignore(256,'\t'); }
    	ss >> current_ac;
		auto it = ac2taxid.find(current_ac);
		if (it == ac2taxid.end()) 
			cerr << "No taxid mapping for " << current_ac << endl;
		current_taxid = it->second;
	} else {
    	for (; i < taxid_column; ++i) { ss.ignore(256,'\t'); }
    	ss >> current_taxid;
	}
	if (previous_read == current_read) {
        if (lca_taxid != current_taxid)
    		lca_taxid = lca(Parent_map, lca_taxid, current_taxid);
	} else {
		if (lca_taxid != 0) {
			cout << previous_read << '\t' << lca_taxid << '\n';
		}
		previous_read = current_read;
		lca_taxid = current_taxid;
	}
  }
  cout << current_read << '\t' << lca_taxid << '\n';
  return 0;
}

