#include "taxdb.h"

#include <iostream>
#include <getopt.h>
#include <set>
#include <unordered_map>

#define no_argument 0
#define required_argument 1 
#define optional_argument 2

using namespace std;

size_t read_column = 0;
size_t taxid_column = 8;
size_t score_column = 3;

int main(int argc, char **argv) {
  const struct option longopts[] =
  {
    {"help",      no_argument,        0, 'h'},
    {0,0,0,0},
  };
  string taxDB_fileName = argv[1];
  string seq_fileName = argv[2];
  string map_fileName = argv[3];

  TaxonomyDB<uint32_t, uint64_t> taxDB(taxDB_fileName);
  unordered_map<uint32_t, uint32_t> Parent_map = taxDB.getParentMap();
  unordered_map<string, uint32_t> scientificNameMap = taxDB.getScientificNameMap();
  unordered_map<string, uint32_t> acs;
 
  string line;
  std::cerr << "Reading seq file " << seq_fileName << endl;
  std::ifstream seqFile(seq_fileName);
  if (!seqFile.is_open())
    throw std::runtime_error("unable to open sequence file");
  while (std::getline(seqFile, line)) {
    if (line[0] == '>') {
        size_t next = line.find(' ');
        if (next == std::string::npos) 
            throw std::runtime_error("AC in line?");
        uint32_t *lca_taxid = new uint32_t(0);
        string ac = line.substr(1, next-1);
        //vector<string> acs1 {ac};
        //all_acs[ac] = lca_taxid;
        //for (size_t start = line.find(']', next+1);
        //     start != std::string::npos;
        //     start = line.find(']', next+1)) {
        //    next = line.find(' ', start+1);
        //    if ( next == std::string::npos ) 
        //        break;
        //    ac = line.substr(start+1, next-start-1);
        //    acs1.push_back(ac);
        //    all_acs[ac] = lca_taxid;
        //}
        acs[ac] = 0;
    }
  }
  seqFile.close();

 
  std::cerr << "Reading map file " << map_fileName << endl;
  std::ifstream mapFile(map_fileName);
  if (!mapFile.is_open())
    throw std::runtime_error("unable to open map file");

  // format: accession       accession.version       taxid   gi
  size_t cnt = 0;
  string ac, ac_v;
  uint32_t taxid, gi;
  std::getline(mapFile, line); // header
  while (mapFile >> ac >> ac_v >> taxid >> gi) {
    auto it = acs.find(ac_v);
    if (it != acs.end()) {
        cout << ac_v << '\t' << taxid << endl;
        ++cnt;
    }
  }
  mapFile.close();
  cerr << "Mapped " << cnt << " out of " << acs.size() << " sequences" << endl;
  return 0;
}

