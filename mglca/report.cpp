
#include <string>
#include <iostream>
#include "taxdb.h"

using namespace std;

int main(int argc, char **argv) {
	if (argc != 2) {
      std::cout << "Provide names.dmp and nodes.dmp\n";
      return 1;
    }
    TaxonomyDB<uint32_t, uint64_t> taxDB(argv[1]);

    unordered_map<uint32_t, uint64_t> to_c;
    string read;
    uint32_t taxid;
    while (cin >> read >> taxid) {
        auto it = to_c.find(taxid);
        //cerr << "Adding to " << taxid << endl;
        if (it == to_c.end()) {
            to_c[taxid] = 1;
        } else {
            ++to_c[taxid];
        }
    }
    taxDB.fillCounts(to_c);

	TaxReport<uint32_t,uint64_t> rep(cout, taxDB, false);
	rep.printReport("kraken","blu");
}
