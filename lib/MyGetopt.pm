package MyGetopt;
use strict;
use warnings;

use Getopt::Long;
use Term::ANSIColor;
use Exporter qw/import/;
our @EXPORT_OK = qw/set_options MyGetopt get_option option_defined system_call/;

my $usage = colored("script [options]","bold");

my (%options, @gopts);

sub set_options {
    my (@opt_spec) = @_;

    foreach my $optt (@opt_spec) {
        $usage .= "\n";
        next unless (scalar(@$optt) ==3);

        (my $optts = $optt->[0]) =~ s/[=!].*//;
        $options{$optts} = $optt->[2];

        $usage .= sprintf("\t%-30s %s %s",
            colored("--".$optt->[0],"green"),
            colored($optt->[1],"reset"),
            colored("[default=".(defined $optt->[2]? $optt->[2] : '<undef>')."]","white"));
        push @gopts,$optt->[0];
    }
    $usage .= "\n";

}

sub MyGetopt {
    set_options(@_);
    GetOptions(\%options, @gopts) || die $usage;
    return(\%options, \@gopts, $usage);
}

sub get_option {
    my ($option) = @_;
    die "$option is not a valid option" if (!defined $options{$option});
    return $options{$option};
}

sub option_defined {
    return defined $options{$_[0]};
}

my $system_col = "white on_black";
sub system_call {
    print colored(" SYSTEM CALL: @_\n" , $system_col);
    if (get_option("pretend")) {

    } else {
        system(@_) == 0
          or die "system @_ failed: $!";
        print colored(" finished\n",$system_col);
    }
}


1;
