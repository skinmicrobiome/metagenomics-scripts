#!/bin/env perl

use strict;
use warnings;
use Term::ANSIColor;
use File::Basename;
use Number::Bytes::Human qw(format_bytes);

use FindBin '$Bin';
use lib "$Bin";
use Utils qw/system_call system_call_f system_call_l get_option option_defined/;
use PipelineConf;
use DBConf;

my %conf_all = %PipelineConf::conf;
my %conf = %{$conf_all{"default"}};
my $checkmark = colored("✓","green");

my $usage = basename($0)." [OPTIONS] TIMESTAMP

TIMESTAMP: Used as timestamp for all created targets and databases. Targets are 
written to DATABASE_DIR/common-TIMESTAMP.

OPTIONS:
";
$usage = 
    Utils::get_options($usage,
        ['target=s', 'Build specified target(s). One of '.join(", ", sort(keys %DBConf::targets)), undef],
        ['all-targets', "Build all targets", 0],
        ['database=s', 'Build specified database(s). One of '.join(", ", sort(keys %DBConf::databases)), undef],
        ['all-databases', "Build all databases", 0],
        [],
        ['force-database', "Force database build", 0],
        ['build-params=s', "Additional building parameters", ""],
        ['main-dir=s',   "Database base directory.", "db" ],
        ['proc=i', "Number of threads", 10],
        ['force', "Force (remove directories first!)", 0]
    );

my $nproc = get_option('proc');
$DBConf::nproc = $nproc;
die "Define main-dir\n$usage" unless (option_defined('main-dir'));
if (scalar(@ARGV) != 1) {
    print STDERR $usage;
    exit 1;
}
my $timestamp = $ARGV[0];

# That was useful for fixing the column 20/21 error in viral genomes assembly_summary.txt:
# cut -f 6,20,21 assembly_summary.txt | sed 's/^\(.*\)\t\(ftp:.*\)\t.*/\1\t\2/;s/^\(.*\)\t.*\t\(ftp:.*\)/\1\t\2/' | while read taxid next; do BN=`basename $next`_genomic_dustmasked.fna; if [[ ! -z $BN ]]; then echo $BN; curl ${next}/`basename $next`_genomic.fna.gz | gunzip -c | sed "s/^>/>kraken:taxid|$taxid /" | dustmasker -infmt fasta -in /dev/stdin -outfmt fasta | sed '/^>/! s/[^AGCT]/N/g' > $BN; fi; done

my $main_dir = get_option("main-dir");
my $common_dir = "$main_dir/common-$timestamp";
my $log_file = basename($0).".log";
reset_log_file();

my $tax_dir = "$common_dir/taxonomy";
if (!-f "$common_dir/taxonomy/taxonomy.complete") {
    system_call("mkdir -p $common_dir/taxonomy");
    system_call("curl ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz -o $tax_dir/taxdump.tar.gz && tar xvvf $tax_dir/taxdump.tar.gz -C $tax_dir");
    system_call("touch $common_dir/taxonomy/taxonomy.complete");
}


my %build_targets;
if (get_option("all-targets")) {
    foreach my $target (keys %DBConf::targets) {
        $build_targets{$target} = 1;
    }
} elsif (option_defined("target")) {
    $build_targets{get_option("target")} = 1;
}

my @build_databases;
if (get_option("all-databases")) {
    @build_databases = sort(keys %DBConf::databases);
} elsif (option_defined("database")) {
    @build_databases = (get_option("database"));
}

## Check all databases and targets are valid
for my $database (@build_databases) {
    die "$database not defined as database" unless defined $DBConf::databases{$database};
    my $db = $DBConf::databases{$database};
    foreach my $target (@{$db->[0]}) {
        $build_targets{$target} = 1;
    }
}

foreach my $target (sort keys %build_targets) {
    die "$target not defined as target" unless defined $DBConf::targets{$target};
}

## build targets and databases
print STDERR colored("BUILDING REQUESTED TARGETS\n", "green bold");
foreach my $target (sort keys %build_targets) {
    build_target($target);
}

print STDERR colored("BUILDING REQUESTED DATABASES\n", "green bold");
for my $database (@build_databases) {
    build_database($database);
}

sub build_target {
    my ($target) = @_;
    print STDERR " --> target $target ";
    my $dir = "$common_dir/$target";
    if (get_option("force") && -d $dir) {
        system_call("rm -rf $dir");
    }

    if (! -s "$dir/$target.fna" || !-s "$dir/seqid2taxid.map") {
        print STDERR "building ...\n";
        system_call("rm -rf $dir && mkdir -p $dir/seqs");
        &{$DBConf::targets{$target}}($dir,$tax_dir);
        system_call_f("find $dir/seqs -iname '*.fna' -o -iname '*.fa' -o -iname '*.fasta' | xargs cat  ","$dir/$target.fna");
        system_call("samtools faidx $dir/$target.fna");
    } else {
        print STDERR "$checkmark\n";
    }
    return if get_option("pretend");
    my $fna_size = -s "$dir/$target.fna";
    my $map_size = -s "$dir/seqid2taxid.map";
    print STDERR "\t$dir/target.fna:      ";
    my $fna_n_seq = `cat $dir/$target.fna.fai | wc -l`; chomp $fna_n_seq;
    print STDERR "$fna_n_seq sequences";
    print STDERR ", ".format_bytes($fna_size)." in total\n";
    print STDERR "\t$dir/seqid2taxid.map: ";
    my $map_lines = `cat $dir/seqid2taxid.map | wc -l`; chomp $map_lines;
    print STDERR $map_lines." mappings\n";
    die "Error in creating fna or seqidmap" unless ($fna_size && $map_size);
}

sub build_database {
    my ($database) = @_;
    print STDERR "Looking at $database ...\n";
    my $db = $DBConf::databases{$database};
    my $database_dir = get_option("main-dir")."/$database-$timestamp";
    if (!get_option("force-database") && -f "$database_dir/all.complete") {
        print STDERR " - $database_dir/all.complete present $checkmark\n";
        next;
    }

    ## restart from scratch
    system_call("rm -rf $database_dir && mkdir -p $database_dir");
    #$Utils::log_file = "$database_dir/build.log";
    ## build command line, call command

    my @fna_files;
    my @seqid2taxid_files;
    foreach my $target (@{$db->[0]}) {
        my $dir = "$common_dir/$target";
        my $fna = "$dir/$target.fna";
        my $seqid2taxid = "$dir/seqid2taxid.map";
        die "$fna empty" unless -s $fna;
        die "$seqid2taxid empy" unless -s $seqid2taxid;
        push @fna_files, $fna;
        push @seqid2taxid_files, $seqid2taxid;
    }

    my $program = dirname($database);

    my $cmd;
    #$cmd =~ s/{main-dir}/$database_dir/;
    #$cmd =~ s/{seqid2taxid-files}/$seqid2taxid_files_cs/;
    #$cmd =~ s/{fna-files}/$fna_files_cs/;

    if ($program eq "kraken") {
        system_call("cat @seqid2taxid_files > $database_dir/seqid2taxid.map");
        system_call("ln -s ".(File::Spec->rel2abs($tax_dir))." $database_dir");
        system_call("touch $database_dir/gi2seqid.map");
        mkdir("$database_dir/library");
        foreach my $fna (@fna_files) {
            system_call("ln -s ".(File::Spec->rel2abs($fna))." $database_dir/library");
        }
        $cmd = (get_conf("kraken","build_bin"))." --db $database_dir --threads $nproc --build";
        $cmd.= " ".($db->[1]) if defined $db->[1];
    } elsif ($program eq "centrifuge") {
        my $seqid2taxid_files_cs = join(",", @seqid2taxid_files);
        my $fna_files_cs = join(",", @fna_files);

        $cmd = $conf{"centrifuge_build_bin"}." --converison-table $seqid2taxid_files_cs";
        $cmd .= " --name-table $tax_dir/names.dmp --taxonomy-tree $tax_dir/nodes.dmp";
        $cmd.= " ".($db->[1]) if defined $db->[1];
        $cmd .= " $fna_files_cs ".(basename($database));
    } else {
        die("Unknown program $program");
    }
    #$cmd .= " ".get_option("build-params") if option_defined("build-params");

    system_call($cmd);
    system_call("touch $database_dir/all.complete");
    #reset_log_file();
}

sub reset_log_file {
    $Utils::log_file = $log_file;
}

sub get_conf1 {
    my $conf1 = shift;
    my $arg1 = shift;
    die "$arg1 not defined in conf" unless (defined $conf1->{$arg1});
    if (scalar(@_) == 0) {
        return($conf1->{$arg1});
    } else {
        return get_conf1($conf1->{$arg1}, @_);
    }
}

sub get_conf {
    return (get_conf1(\%conf, @_));
}


