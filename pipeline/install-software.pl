#!/bin/env perl

use strict;
use warnings;

use File::Basename;
use FindBin '$Bin';
use lib "$Bin";
use SampleDefinitions;
use PipelineConf;
use Utils qw/system_call get_option/;
use Term::ANSIColor;
use File::Path "make_path";

my $timestamp;
$timestamp = `date +"%y%m%d"` unless defined $timestamp;
chomp $timestamp;

$Utils::log_file = "install.log";

my %conf_all = %PipelineConf::conf;
my %conf = %{$conf_all{"default"}};

sub install_program {
    my $program_name = shift;
    my $program = get_conf($program_name);

    my $version = $program->{"version"};
    my $dir = "sw/$program_name-v$version";
    system_call("rm -rf $dir && mkdir -p $dir");
    my $cmd = $program->{"install_cmd"};

    $cmd =~ s/{dir}/$dir/g;
    for my $k (keys %$program) {
        my $v = $program->{$k};
        $cmd =~ s/{$k}/$v/g;
    }
    die "Couldn't completely translate $cmd" if ($cmd =~ /[{}]/); 
    system_call($cmd);
}

my %progs = (
    spades => sub { install_program("spades");
    },
    blast => sub {
        my $dir = get_conf("blast_dir");
        my $v = "2.6.0";
        if (!check_mkdir($dir)) {
            system_call("wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/$v/ncbi-blast-$v+-src.tar.gz -O $dir/ncbi-blast-$v+-src.tar.gz");
            system_call("tar xvvf $dir/ncbi-blast-$v+-src.tar.gz --strip-components -C $dir");
            system_call("cd $dir && ./configure && make && mv SLAM ..");
        }
    },
    diamond_nr_database => sub {
        my $dir = "db/diamond";
        mkdir($dir) unless -d $dir;
        system_call("curl ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nr.gz | gunzip -c > $dir/nr-$timestamp.faa");
        system_call(get_conf("diamond_bin")." makedb --in $dir/nr-$timestamp.faa -d $dir/nr-$timestamp");
        ## TODO: Add mapping file!!
    },
    diamond_swissprot_database => sub {
        my $dir = dirname(get_conf("diamond_swissprot_database"));
        mkdir($dir) unless -d $dir;
        system_call("curl ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/swissprot.gz | gunzip -c > $dir/swissprot.faa");
        system_call(get_conf("diamond_bin")." makedb --in $dir/swissprot-$timestamp.faa -d $dir/swissprot-$timestamp");
    },

    kraken => sub {
        my $dir = get_conf("kraken_dir");
        clone_or_pull_git($dir, "-b taxid-bitmap https://github.com/fbreitwieser/kraken.git");
        system_call("cd $dir && ./install_kraken.sh `pwd`/install");
    },
    centrifuge => sub {
        my $dir = get_conf("centrifuge_dir");
        clone_or_pull_git($dir, "https://github.com/infphilo/centrifuge.git");
        system_call("cd $dir && make allall");
    },
    centrifuge_nt_database => sub {
        my $dir = dirname(get_conf("centrifuge_nt_database"));
        dl_and_extract("$dir/nt.tar.gz", "ftp://ftp.ccb.jhu.edu/pub/infphilo/centrifuge/data/nt.tar.gz", 1);
    },

    kslam => sub {
        my $dir = get_conf("kslam_dir");
        clone_or_pull_git($dir, "https://github.com/aindj/k-SLAM");
        system_call("cd $dir/build/ && make && cd -");
        system_call("mv $dir/build/SLAM $dir/SLAM");
    },

    kslam_pv_database => sub {
       system_call(get_conf("kslam_build_script"),"db/kslam/bv-$timestamp","bacteria viruses");
    },

    bbtools => sub{
        my $dir = get_conf("bbtools_dir");
        dl_and_extract("$dir/BBMap_36.92.tar.gz", "https://sourceforge.net/projects/bbmap/files/BBMap_36.92.tar.gz", 1, "--strip-components 1");
    },


    test => sub { 
        system_call("while [ 1 == 1 ]; do echo i && sleep 3; done");
    }
);

my $usage = colored(basename($0)." [OPTIONS] TARGETs\n","bold").
"TARGETS: 
\t".join(", ", sort keys %progs)."
OPTIONS:";

$usage = Utils::get_options($usage);

if (scalar(@ARGV) == 0) {
    print $usage;
    exit 0;
}


foreach my $prog (@ARGV) {
    if (!defined $progs{$prog}) {
        print "Target $prog does not exist !! \n";
        print $usage;
        exit 1;
    }
}

foreach my $prog (@ARGV) {
    print "Executing target $prog ... \n";
    &{$progs{$prog}}();
}



###############

sub check_mkdir {
    my ($dir) = @_;
    if (-d $dir) {
        print STDERR "$dir exists.\n";
        return 1;
    } else {
        print STDERR "$dir does not exist - creating it.\n";
        system_call("mkdir -p $dir");
        return 0;
    }
}

sub dl_and_extract {
    my ($file, $url, $rm_dir, $tar_args) = @_;
    my $dir = dirname($file);
    if ($rm_dir) {
        system_call("rm -rf $dir");
    }
    if (!check_mkdir($dir)) {
        system_call("wget $url -O $file");
        system_call("tar xvvf $file -C $dir".(defined $tar_args? " $tar_args" : ""));
    }
}

sub clone_or_pull_git {
    my ($dir, $git_repo) = @_;

    if (-d $dir) {
        system_call("cd $dir && git pull && cd -");
    } else {
        system_call("git clone $git_repo $dir");
    }
}



sub get_conf {
    my ($c) = @_;
    die "$c is not specified in the configuration (PipelineConf.pm)" if (!defined $conf{$c});
    return $conf{$c};
}


