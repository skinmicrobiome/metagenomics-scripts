package DBConf;

use FindBin '$Bin';
use Utils qw/system_call system_call_f system_call_l get_option option_defined/;

my $scripts_dir = "/home/fbreitwieser/projects/metagenomics-scripts/db-scripts";
my $seq_dir = "seqs";
our $nproc = 1;

sub make_seqid_map {
    my ($dir, $taxid) = @_;
    system_call_f("find $dir/$seq_dir -iname '*.fna' -o -iname '*.fa' -o -iname '*.fasta' | xargs grep -h '^>' | sed -e 's/^>//' -e 's/ .*//' -e 's/\$/\\t$taxid/'","$dir/seqid2taxid.map");
}

our %databases = (
    "kraken/filter_k25" => [
        ['human','mmus','contaminants'],
        "--jellyfish-hash-size 10000M --kmer-len 25 --minimizer-len 12"
    ],
    "kraken/archaea_k25" => [
        ['archaea'],
        "--jellyfish-hash-size 10000M --kmer-len 25 --minimizer-len 12"
    ],
    "kraken/a+b+h+v" => [
        ['human','contaminants', 'bacteria', 'archaea', 'all-viral', 'viral-strains'],
        "--jellyfish-hash-size 40000M"
    ],
    #"centrifuge/p+h+v" => [
    #    ['human','contaminants', 'bacteria', 'archaea', 'all-viral', 'viral-strains'],
    #],
    "kraken/a+b" => [
        ['bacteria', 'archaea'],
    ],
    "kraken/all-a+b" => [
        ['all-bacteria', 'all-archaea'],
    ],
    "kraken/f+p" => [
        ['all-protozoa', 'all-fungi'],
        "--generate-taxonomy-ids-for-sequences"
    ],
    "kraken/p+v" => [
        ['bacteria', 'archaea', 'all-viraa', 'viral-strains', 'viroid-strains'],
    ],
    "kraken/v_k25" => [
        ['all-viral', 'viral-strains', 'viroid-strains'],
        "--jellyfish-hash-size 40000M --kmer-len 25 --minimizer-len 12"
    ],
    "kraken/v" => [
        ['all-viral', 'viral-strains', 'viroid-strains'],
        "--jellyfish-hash-size 40000M"
    ],
 
#    "p+v" => [[],"{kraken-build} --db {main-dir} --build --jellyfish-hash-size 40000M"]
);

our %targets = (
    "human" => sub {
        my ($dir) = @_;
        system_call_f("curl ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/human_genomic.gz | gunzip -c","$dir/$seq_dir/human_genomic.fna");
        system_call_f("curl ftp://ftp.ncbi.nlm.nih.gov/genomes/Homo_sapiens/RNA/rna.fa.gz | gunzip -c","$dir/$seq_dir/human_rna.fna");
        make_seqid_map("$dir",9606);
    },
    "mmus"     => sub { 
        my ($dir) = @_;
        system_call("$scripts_dir/download-mmus-reference-genome $_[0]/$seq_dir"); 
        make_seqid_map("$dir",10090);
    },
    "athaliana" => {
        my $dir = @_;
        system_call_f("curl ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/735/GCF_000001735.3_TAIR10/GCF_000001735.3_TAIR10_genomic.fna.gz | gunzip -c", "$dir/$seq_dir/GCF_000001735.3_TAIR10_genomic.fna.gz");
        system_call_f("curl ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/735/GCF_000001735.3_TAIR10/GCF_000001735.3_TAIR10_rna.fna.gz | gunzip -c", "$dir/$seq_dir/GCF_000001735.3_TAIR10_rna.fna.gz");
        make_seqid_map($dir, 3702);
    },
    "contaminants" => sub { 
        my ($dir) = @_;
        system_call_f("curl ftp://ftp.ncbi.nlm.nih.gov/pub/UniVec/UniVec","$dir/$seq_dir/UniVec.fna");
        make_seqid_map("$dir",32630);
    },
    "bacteria" => sub { system_call_f("$scripts_dir/centrifuge-download -o $_[0]/$seq_dir -P $nproc -d bacteria -a 'Complete Genome' -m refseq","$_[0]/seqid2taxid.map"); },
    "archaea" => sub { system_call_f("$scripts_dir/centrifuge-download -o $_[0]/$seq_dir -P $nproc -d archaea -a 'Complete Genome' -m refseq","$_[0]/seqid2taxid.map"); },
    "all-bacteria" => sub { system_call_f("$scripts_dir/centrifuge-download -o $_[0]/$seq_dir -P $nproc -d bacteria -a 'Any' -m refseq","$_[0]/seqid2taxid.map"); },
    "all-archaea" => sub { system_call_f("$scripts_dir/centrifuge-download -o $_[0]/$seq_dir -P $nproc -d archaea -a 'Any' -m refseq","$_[0]/seqid2taxid.map"); },
    "all-viral" => sub { system_call_f("$scripts_dir/centrifuge-download -o $_[0]/$seq_dir -P $nproc -d viral -a 'Any' -m refseq","$_[0]/seqid2taxid.map"); },
    "all-fungi" => sub { system_call_f("$scripts_dir/centrifuge-download -o $_[0]/$seq_dir -P $nproc -d fungi -a 'Any' -m refseq","$_[0]/seqid2taxid.map"); },
    "all-protozoa" => sub { system_call_f("$scripts_dir/centrifuge-download -o $_[0]/$seq_dir -P $nproc -d protozoa -a 'Any' -m refseq","$_[0]/seqid2taxid.map"); },
    "viral-strains" => sub {
        my ($db_dir,$tax_dir) = @_;
        die "TaxDir??" unless -d $tax_dir;
        system_call_f("$scripts_dir/download-viral-genomes.pl $db_dir viral $nproc $tax_dir/names.dmp", "$db_dir/seqid2taxid.map");
    },
    "viroid-strains" => sub {
        my ($db_dir,$tax_dir) = @_;
        die "TaxDir??" unless -d $tax_dir;
        system_call_f("$scripts_dir/download-viral-genomes.pl $db_dir viroid $nproc $tax_dir/names.dmp", "$db_dir/seqid2taxid.map");
    }

);



1;
