package Ts;

# packaged'd version of ts Copyright 2006 by Joey Hess <id@joeyh.name>
# Licensed under the GNU GPL.

# ts is distributed in the moreutils package

use warnings;
use strict;
use POSIX q{strftime};
no warnings 'utf8';

$|=1;

our %conf = (
    rel => 0,
    inc => 0,
    sincestart => 0,
    format => "%b %d %H:%M:%S",
    format_inc => "%H:%M:%S"
);

my $hires=0;
my $lastseconds = time;
my $lastmicroseconds = 0;

sub setup {
    if ($conf{"format"}=~/\%\.[Ss]/) {
        require Time::HiRes;
        $hires=1;
    }
    if ($hires) {
        ($lastseconds, $lastmicroseconds) = Time::HiRes::gettimeofday();
    } else {
        $lastseconds = time;
    }
}

sub ts {
    if (! $rel) {
        if ($hires) {
            my $f=$format;
            my ($seconds, $microseconds) = Time::HiRes::gettimeofday();
            if ($inc || $sincestart) {
                my $deltaseconds = $seconds - $lastseconds;
                my $deltamicroseconds = $microseconds - $lastmicroseconds;
                if ($deltamicroseconds < 0) {
                    $deltaseconds -= 1;
                    $deltamicroseconds += 1000000;
                }
                if ($inc) {
                    $lastseconds = $seconds;
                    $lastmicroseconds = $microseconds;
                }
                $seconds = $deltaseconds;
                $microseconds = $deltamicroseconds;
            }
            my $s=sprintf("%06i", $microseconds);
            $f=~s/\%\.([Ss])/%$1.$s/g;
            print strftime($f, localtime($seconds));
        }
        else {
            if ($inc || $sincestart) {
                my $seconds = time;
                my $deltaseconds = $seconds - $lastseconds;
                if ($inc) {
                    $lastseconds = $seconds;
                }
                print strftime($format, localtime($deltaseconds));
            } else {
                print strftime($format, localtime);
            }
        }
        print " ",@_;
    }
    else {
        s{\b(
            \d\d[-\s\/]\w\w\w   # 21 dec 17:05
                (?:\/\d\d+)?    # 21 dec/93 17:05
                [\s:]\d\d:\d\d  #       (time part of above)
                (?::\d\d)?  #       (optional seconds)
                (?:\s+[+-]\d\d\d\d)? #  (optional timezone)
            |
            \w{3}\s+\d{1,2}\s+\d\d:\d\d:\d\d # syslog form
            |
            \d\d\d[-:]\d\d[-:]\d\dT\d\d:\d\d:\d\d.\d+ # ISO-8601
            |
            (?:\w\w\w,?\s+)?    #       (optional Day)
            \d+\s+\w\w\w\s+\d\d+\s+\d\d:\d\d:\d\d
                        # 16 Jun 94 07:29:35
                (?:\s+\w\w\w|\s[+-]\d\d\d\d)?
                        #   (optional timezone)
            |
            \w\w\w\s+\w\w\w\s+\d\d\s+\d\d:\d\d
                        # lastlog format
          )\b
        }{
            $use_format
                ? strftime($format, localtime(str2time($1)))
                : concise(ago(time - str2time($1), 2))
        }exg;

        print @_;
    }
}

1;
