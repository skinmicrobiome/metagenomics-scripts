#! /usr/bin/env perl
#
# run-pipeline.pl picks up files in the staging directory,
#  and processes them according to information encoded in their
#  file name
#
# Author fbreitwieser <fbreitwieser@igm3>
# Version 1.0
# Copyright (C) 2017 fbreitwieser <fbreitwieser@igm3>
# Modified On 2017-02-05 16:06
# Created  2015-04-21 16:06
#
use strict;
use warnings;

use File::Basename;
use File::Copy;
use File::Path "make_path";
use File::stat;
use Term::ANSIColor;
use Getopt::Long;
use Number::Bytes::Human qw(format_bytes);
use Data::Dumper;

use FindBin '$Bin';
use lib "$Bin";
use SampleDefinitions;
use PipelineConf qw/get_conf/;
use Utils qw/get_option option_defined system_call/;
use Text::Wrap;
use Term::ReadKey;
my ($wchar, $hchar, $wpixels, $hpixels) = GetTerminalSize();
use 5.14.0;

$ENV{SHELL}='bash -o pipefail -euc';
$ENV{PERL5SHELL}='bash -o pipefail -euc';
$ENV{BLASTDB}=".:db/blastp:db/blastn:".$ENV{BLASTDB};

my %did_warn;
my $system_col = "white on_black";

my %sample_type = %SampleDefinitions::sample_type;
foreach my $s (keys %sample_type) {
    if (!defined ($sample_type{$s}->{"staging_dir"})) {
        $sample_type{$s}->{"staging_dir"} = "fastq-files/$s";
    }
}
my @sample_types = sort keys %sample_type;

my @kraken_dbs = map { s/[^\/]*$//r } (glob("db/kraken/*/database.kdb"));
@kraken_dbs = map { s#^db/([^/]*)#colored($1, "yellow")#er } @kraken_dbs;
my @kslam_dbs = map { s/[^\/]*$//r } glob("db/kslam/*/database");
@kslam_dbs = map { s#^db/([^/]*)#colored($1, "bright_magenta")#er } @kslam_dbs;
my @cf_dbs = map { s/.1.cf$//r } glob("db/centrifuge/*/*.1.cf");
@cf_dbs = map { s#^db/([^/]*)#colored($1, "magenta")#er } @cf_dbs;
my @blastn_dbs = map { s/.[np]al$//r } glob("db/blastn/*.[np]al");
@blastn_dbs = map { s#^db/([^/]*)#colored($1, "cyan")#er } @blastn_dbs;
my @blastx_dbs = map { s/.[np]al$//r } glob("db/blastx/*.[np]al");
@blastx_dbs = map { s#^db/([^/]*)#colored($1, "bright_cyan")#er } @blastx_dbs;
my @diamond_dbs = map { s/.dmnd$//r } glob("db/diamond/*.dmnd");
@diamond_dbs = map { s#^db/([^/]*)#colored($1, "bright_yellow")#er } @diamond_dbs;

my @analyses = (@blastn_dbs,@blastx_dbs,@cf_dbs,@diamond_dbs,@kraken_dbs,@kslam_dbs);
@analyses = map { s#^db.##r } @analyses;
@analyses = map { s#/$##r } @analyses;

#my $host_name = `hostname`; chomp $host_name;
my %conf_all = %PipelineConf::conf;
#die "Configuration for host '$host_name' is not defined!" unless defined $conf_all{$host_name};
#my %conf = %{$conf_all{$host_name}};
my %conf = %{$conf_all{"default"}};

-d "locks" or `mkdir -p locks`;
my $checkmark = colored("✓", "green");
my $fastq_to_fasta = "sed -n '1~4s/^@/>/p;2~4p'";
my $processing_dir = "processing";

my %file_sizes;
my %analyzed_files;

$Text::Wrap::columns = $wchar -5;

my %presets = (
    std => {
        results_dir => "processing",
        filter => ['kraken db/kraken/filter_k25-170210', 'q_filter']
    }
);

print "@analyses\n";

my $usage = colored(basename($0)." [OPTIONS] SAMPLE_TYPE ANALYSES\n", "bold")
.wrap("SAMPLE_TYPEs (use 'list' for details): ", "  ", join(" ",  map {colored($_,"blue")} @sample_types))."\n"
.wrap("ANALYSES: ","  ",join(" ", @analyses))
."\nOPTIONS:";

my $e001 = 1;

$usage = Utils::get_options($usage,
    ['force', "regenerate files", 0 ],
    ['threads=i', "number of threads", 10 ],
    ['pattern=s', "Only take samples following a pattern", undef],
    ['q-filter', "quality-filter extracted reads", 0],
    ['not=s', "Align against reads that are not matched against db", undef],
    ['extract-reads=s', "Extract reads that match pattern. E.g. 'Homo sapiens'", undef],
    ['subset=s', "Classify only a subset of reads. s must be PROG/DB/PATTERN, e.g. 'megablast/nt/Homo sapiens'.", undef ],
#    [],
    ['filter-w-kraken=s', "Filter all reads matching to specified Kraken database", "db/kraken/filter_k25-170210", 0],
    ['ignore-locks', "Process all files, ignoring locks", 0],
#    ['use-kraken-db=s', "Use a different kraken DB then the default one (specified in conf). The result files will get the basename of the DB appended.", undef],
#    [],
#    ['blastx!', "run blastx", 0],
#    ['centrifuge', "run centrifuge", 0],
#    ['centrifuge2', "run centrifuge2", 0],
#    ['humann2', "run humann2", 0],
#    ['hisat2', "run hisat2", 0],
#    ['kallisto', "run kallisto", 0],
#    ['bracken', "run Bracken", 0],
#    ['kslam', "run k-SLAM", 0],
#    [],
    ['align-to=s', "align samples to genome idx", undef],
#    ['extract-reads=s', "extract reads for specific tax ids", undef],
#    ['extract-reads-name=s', "Name for extract reads for specific tax ids", undef],
#    ['extract-unidentified-reads', "Get the unidentified fraction (requires a previous Kraken run)", 0],
#    ['extract-nonhuman-reads', "extract reads that are neither human nor synthetic constructs", 0],
#    ['against-unidentified-reads', "Run the search against the unidentified fraction (requires a previous Kraken run)", 0],
    ## Hidden options
    #['use-argv', "use FASTQ provided as arguments", 0],
    #['extended-kraken-report', "call kraken-report-modif", 0],
    #['filter!', "filter human, mouse, and rRNA with bowtie2 beforehand?", 0 ],
    #['sample-type=s', "sample type", "" ],
    #['staging-dir=s', "staging directory", 'staging' ],
    #['check-files!', "check if files are being modified", 0, 0 ],
    #['force-kraken', "force running kraken", 0],
    #['force-kraken-report', "force running kraken-report", 0],
    #['interactive', "interactive mode", 0],
    #['reverse', "reverse processing input files", 0]
);

#my $usage = colored("run-pipeline.pl [options]", "bold");

my $threads = get_option("threads");
my $bam_params = "--threads $threads";

sub get_sample_type_files {
    my ($my_sample_type) = @_;
    my @files;
	opendir(my $STAGING_DIR, $my_sample_type->{"staging_dir"}) or return @files;
    foreach my $fastq_file (sort readdir($STAGING_DIR)) {
        next if ($fastq_file =~ m/^\./);
	    next unless ($fastq_file =~ /$my_sample_type->{ext}$/);

        #next if ($my_sample_type->{paired} && !($fastq_file =~ /_1/));
        my $basename = basename($fastq_file, $my_sample_type->{'ext'});
        my $pattern = $my_sample_type->{fname_pattern};
        $pattern =~ s/%/1/ if $my_sample_type->{paired};
        my @re_res = ($basename =~ /^$pattern$/);

        if (option_defined("pattern")) {
            my $fname_pattern = get_option("pattern");
            next unless $fastq_file =~ /$fname_pattern/;
        }

        next unless (scalar @re_res >= 1);
        push @files, $fastq_file;
    }
    close ($STAGING_DIR);
    return @files;
}

my $sample_type_name = shift;
if ($sample_type_name eq "list") {
    print "Available sample types: \n";
    foreach my $s (sort keys %sample_type) {
        my $dir = $sample_type{$s}->{"staging_dir"};
        print colored($s, "blue"), " in ",
            colored($dir, (-d $dir ? "green" : "red"));
        print " [", (scalar get_sample_type_files($sample_type{$s})), " matching files]", "\n";
    }
    exit;
}
if (! defined $sample_type_name) {
    print $usage;
    exit;
}
print colored("Give a valid sample type \n", "red").$usage, exit unless defined $sample_type{$sample_type_name};


my @programs = @ARGV;

#foreach my $sample_type_name (@ARGV) {
{
    my $my_sample_type=$sample_type{$sample_type_name};
    my $staging_dir = $my_sample_type->{'staging_dir'};

    Utils::print_and_log_msg colored("ANALYZING $sample_type_name (staging dir: $staging_dir/)", "green");

    my $cnt = 0;
	opendir(my $STAGING_DIR, $staging_dir) or die $1;
    my @files = sort readdir($STAGING_DIR);

    if (option_defined("pattern")) {
        my $fname_pattern = get_option("pattern");
        @files = grep(/$fname_pattern/, @files);
    }

    my $ext = $my_sample_type->{ext};
    my $pattern = $my_sample_type->{fname_pattern};
    $pattern =~ s/\$?$/$ext/;
    if ($my_sample_type->{paired}) {
        (my $pattern1 = $pattern) =~ s/%/1/;
        (my $pattern2 = $pattern) =~ s/%/2/;
        my @files1 = grep(/$pattern1/, @files);
        my @files2 = grep(/$pattern2/, @files);

        ## Check if all the files are paired correctly
        if (scalar(@files1) != scalar(@files2)) {
            die "Not all files correctly paired!!";
        }
        for (my $i=0; $i < scalar(@files1); ++$i) {
            my $f1 = $files1[$i];
            my $f2 = $files2[$i];
            $f1 =~ s/1/%/g;
            $f2 =~ s/2/%/g;
            die ("$f1 is not paired with $f2 !!") unless $f1 eq $f2;

            do_process($sample_type_name, ["$staging_dir/$f1", "$staging_dir/$f2"], parse_file($my_sample_type,$f1));
        }
    } else {
        @files = grep(/$pattern/,@files);
	    foreach my $f (@files) {
            do_process($sample_type_name, ["$staging_dir/$f"], parse_file($my_sample_type, $f));
    	}
    }

    closedir($STAGING_DIR);
}

sub parse_file {
    my ($my_sample_type, $file) = @_;
    my $basename = basename($file, $my_sample_type->{'ext'});

    ## make sure file has the correct things encoded in its name and extract it
    my $pattern = $my_sample_type->{'fname_pattern'};
    my @re_res = ($basename =~ /^$pattern$/);
    if (scalar @re_res < 1) {
        die "! $basename =~ $pattern: @re_res\n";
    }

    my $sample_code = $re_res[0];

    my $seq_type = $my_sample_type->{sequencing_type};
    if ($seq_type ne 'RNA' && $seq_type ne 'DNA') {
        $seq_type = $re_res[$seq_type];
    }
    die "seq_type = $seq_type? " unless ($seq_type eq 'RNA' or $seq_type eq 'DNA');
    my $is_rnaseq = $seq_type eq "RNA";

    my $basename1 = $basename;
    my $is_paired = $my_sample_type->{"paired"};
    $basename1 =~ s/[_-]R?1// if $is_paired;   ## basename1 is the combined name, i.e. one for a mate pair

    return ($basename, $basename1, $sample_code, $is_rnaseq, $is_paired);
}

############################ SUBS

sub print_red { print colored("@_", "red"); }
sub print_green { print colored("@_", "green"); }
sub print_blue { print colored("@_", "blue"); }

sub do_process {
    my (undef, undef, undef, $basename) = @_;
    next if $analyzed_files{$basename};

    #next if ($is_rnaseq);
    my $lock_file = "locks/$basename.lock";
    if (-f $lock_file && !get_option("ignore-locks")) {
        Utils::print_and_log_msg "Skipping $basename, lock file $lock_file exists";
    } else {
        `date > $lock_file`;
        do_process1(@_);
        `rm -v $lock_file`;
    }
    $analyzed_files{$basename} = 1;
}

sub warn_once {
    my ($fastq_file, $msg) = @_;
    if (!defined $did_warn{$fastq_file}) {
        print colored "$msg\n", "red";
        $did_warn{$fastq_file} = 1;
    }
    return 0;
}

sub do_process1 {
    my ($sample_type_name, $fastq_files, $basename, $basename1, $sample_code, $is_rnaseq, $is_paired) = @_;

    Utils::print_and_log_msg ("Checking $sample_type_name/".colored($basename, "yellow").": ");

    ## create directory structure
    my $sample_dir = "$processing_dir/$sample_type_name/$sample_code";

    ## run filter
    my $krakendb = get_option("filter-w-kraken");
    my $krakendb_name = basename($krakendb);

    my $unclassified_fq = "$sample_dir/fastq/$basename1.$krakendb_name.unclassified.fq.gz";
    #if (!$is_paired) {
    #    $kraken_res_file = run_program("kraken", $fastq_files, [ "$sample_dir/kraken/$basename1.$krakendb_name.kraken" ],
    #                                   { database => $krakendb, "extra-opts" => "--quick --unclassified-out $unclassified_fq" });
    #} else {
    #    die "Implement paired-end\n";
    #} 
    ## quality filtering
    # my $filtered_unclassified_fq = [ "$sample_dir/fastq/$basename.$krakendb_name.unclassified.fq.gz" ];
    # run_program("bbduk", [ $unclassified_fq ], [ $filtered_unclassified_fq, $filtered_unclassified_fq ]);

    my $res_basename = "$sample_dir/kraken/$basename1.$krakendb_name.kraken";
    run_program("kraken", $fastq_files, $res_basename, { database => $krakendb, "extra-opts" => "--quick"});
    my @unclassified_files = extract_reads("kraken", $fastq_files, $res_basename, 0, "$sample_dir/fastq/$basename.$krakendb_name", 1);
    my @trimmed_files = qtrim_reads(\@unclassified_files);
    $fastq_files = \@trimmed_files;

    ## filter w/ bacteria DB
    #my $db2 = "p-170210";
    #my $kraken_res_file2 = run_program("kraken", $fastq_files, "$sample_dir/kraken/$basename1.$db2.kraken", { database => "db/kraken/$db2"});
    #@filtered_files = extract_kraken_reads($fastq_files, $kraken_res_file2, 0, "$sample_dir/fastq/$basename.after_filter2.qtrimmed");
    #$fastq_files = \@filtered_files;

    my $suffix = "";
    if (option_defined("subset")) {
       foreach  my $subset (split(/,/,get_option("subset"))) {
            ( my $prog_db = $subset ) =~ s/(.*)\/.*/$1/;
            my ($prog,$db,$tax,$ext) = split(/\//, $subset);
            die "prog ok cf??" if $prog eq "centrifuge";
            die "subset option error" unless defined $tax;
            my $res_basename = "$sample_dir/$prog/$basename1.$db.$prog";
            my $res_file = run_program($prog, $fastq_files, $res_basename, { database => "db/$prog_db" });

            (my $tax_ = $tax) =~ s/ /_/g;
            my $is_taxid = ( $tax =~ s/^taxid// );
            $res_basename .= ".$ext" if defined $ext;
            my @filtered_files = extract_reads($prog, $fastq_files, $res_basename, $tax, "$sample_dir/fastq/$prog/$basename1.$db.$prog", $is_taxid);
            if ($tax eq "0") {
                #    $basename1 .= ".unclassified_${prog}_${db}";
                #} else {
                $basename1 .= ".only_${prog}_${db}_$tax_";
            }
            $basename1 .= "_$ext" if defined $ext;

            $fastq_files = \@filtered_files;
        }
    }

    foreach my $prog_db (@programs) {
        $prog_db =~ s#^db##;
        my ($prog,$db) = split(/\//, $prog_db);
        die unless defined $db;
        my $res_basename = "$sample_dir/$prog/$basename1.$db.$prog";
        run_program($prog, $fastq_files, $res_basename, { database => "db/$prog_db" });
        if (option_defined("extract-reads")) {
            my ($tax,$ext) = split(/\//, get_option("extract-reads"));
            extract_reads($prog, $fastq_files, "$res_basename".(defined $ext? ".$ext":""), 
                $tax, "$sample_dir/fastq/$prog/$basename1.$db.$prog".(defined $ext? ".$ext":""));
        }
    }
    
    print " \n";
   
}

sub bt {
    Utils::print_and_log_msg("BT ".join("", @_));
    `@_`;
}

sub extract_reads {
    my ($prog, $fastq_files, $res_basename, $extract_pattern, $extract_basename, $is_taxid) = @_;
    mkdir dirname($extract_basename) unless -d dirname($extract_basename);
    $is_taxid = 0 unless defined $is_taxid;

    Utils::print_and_log_msg(colored("extracting reads","black bold on_white")." with ".($is_taxid? "taxid" : "pattern")." $extract_pattern", "magenta");
    foreach (@$fastq_files) { check_file($_, 0, "  Input:  "); }

    (my $extract_pattern_ = $extract_pattern) =~ s/ /_/g;
    if ($is_taxid) {
        $extract_basename .= ".taxid$extract_pattern_";
    } else {
        $extract_basename .= ".$extract_pattern_";
    }
    my @ex = split(/,/, $extract_pattern);
    $extract_pattern = join("/||/", @ex);

    my $extract_reads_conf = get_conf($prog)->{"extract_reads"};
    die "extract_reads for $prog??" unless defined $extract_reads_conf;
    my ($ext, $readid_column, $taxid_column) = @$extract_reads_conf{("ext","readid_column","taxid_column")};
    die "Mehh" unless defined $taxid_column;

    check_file("$res_basename.report", 0, "  Input:  ") unless $is_taxid;
    check_file("$res_basename$ext", 0, "  Input:  ");
    
    if (!get_option("pretend")) {
        die "$res_basename.report does not exist!" unless -f "$res_basename.report";
        die "$res_basename$ext does not exist!" unless -f "$res_basename$ext";
    }

    if (!-s "$extract_basename.fq.gz"){
        my $taxid_search;
        if (!$is_taxid) {
            my $n_reads = bt("awk -F'\\t' '/$extract_pattern/ {print \$3}' $res_basename.report | paste -sd+ | bc");
            my @taxids = bt("awk -F'\\t' '/$extract_pattern/ {print \$7}' $res_basename.report");
            chomp @taxids; chomp $n_reads;
            die "No taxids!" if scalar(@taxids) == 0;
            $taxid_search = join("\"|| \$$taxid_column == \"", @taxids);
            print STDERR "Extracting from taxonomy IDs @taxids - should get $n_reads back\n";
        } else {
            $taxid_search = $extract_pattern;
        }
        die "taxid_search??" unless defined $taxid_search;
        if ($ext =~ /.gz$/) {
            system_call("gunzip -c $res_basename$ext | awk '\$$taxid_column == \"$taxid_search\" {print \$$readid_column}' > $extract_basename-readids.txt");
        } else {
            system_call("awk '\$$taxid_column == \"$taxid_search\"/ {print \$$readid_column}' $res_basename$ext > $extract_basename-readids.txt");
        }
        check_file("$extract_basename-readids.txt", 1, "Read Ids: ");
    #die "Na: ".scalar(@readids) unless scalar @readids == $n_reads;
        die "PaireD!!!" if scalar(@$fastq_files) > 1;
        system_call("seqtk subseq @$fastq_files $extract_basename-readids.txt | gzip -c > $extract_basename.fq.gz");
    }
    check_file("$extract_basename.fq.gz", 0, "  Result: ");
    return ("$extract_basename.fq.gz");
}

sub qtrim_reads {
    my ($fastq_files) = @_;
    my $is_paired = (scalar(@$fastq_files) == 2);

    Utils::print_and_log_msg(colored("qtrim w/ bbduk","black bold on_white"), "magenta");
    foreach (@$fastq_files) { check_file($_, 0, "  Input:  "); }
    my @trimmed_fastq_files = map { s/\.fq\.gz$/.q_trimmed.fq.gz/r } @$fastq_files;

    if (!-s "$trimmed_fastq_files[0]") {
        my $cmd = get_conf("bbduk_bin")." in=$fastq_files->[0] out=$trimmed_fastq_files[0]"
                    ." threads=$threads ref=".get_conf("bbtools_dir")."/resources/adapters.fa"
                    ." qtrim=rk trimq=25 qtrim=rl trimq=5 entropy=0.5 minavgquality=20";
        $cmd .= " in2=$fastq_files->[1] out2=$trimmed_fastq_files[1]" if $is_paired;
        $cmd .= " 2>&1 | tee $trimmed_fastq_files[0].log ";
        system_call($cmd);
    }

    check_file($trimmed_fastq_files[0], 0, "  Result: ");
    return (@trimmed_fastq_files);
}

sub check_file {
    my ($file, $show_l, $name) = @_;
    $show_l = 0 unless defined $show_l;
    $name = "" unless defined $name;
    if (!get_option("pretend")) {
        my $outfile_size = -s $file;
        if ( file_isnt_ok($file)) {
            die_r("Error creating $file!");
        } else {
            my $nice_bytes = format_bytes($outfile_size);
            $file = colored($file, "underline") if ($name eq "  Result: ");
            if ($show_l) {
                my $n_lines = `cat $file | wc -l`; chomp $n_lines;
                Utils::print_and_log_msg("$name$file [$n_lines lines, $nice_bytes] $checkmark");
            } else {
                Utils::print_and_log_msg("$name$file [$nice_bytes] $checkmark");
            }
        }
    }
    return (1);
}

sub get_p {
    my ($s, $p) = @_;
    $s =~ s/%/$p/ if defined $p;
    return $s;
}

sub is_newer {
    my ($file1, $file2) = @_;
    ## -C returns the age of the file, i.e. test if file1 is younger
    return -C $file1 < -C $file2;
}

sub file_isnt_ok {
    my ($file, $parent_file, $force_option) = @_;

    if (get_option("verbose")) {
        Utils::print_and_log_msg "Checking if $file exists ...";
    }

    my $positive_result = 0;
    if (get_option("force") || (defined $force_option && option_defined($force_option))) {
        Utils::print_and_log_msg "  --force specified, (re)creating file in any way.";
        $positive_result = 1;
    }

    if (!-f $file) {
        Utils::print_and_log_msg "  ".colored($file, "underline")." does not exist - creating it ... ";
        return 1;
    }
    if (!-s $file) {
        Utils::print_and_log_msg "  ".colored($file,"underline")." exists, but has zero size. Recreating ...";
        return 1;
    }
    if (defined $parent_file && is_newer($parent_file, $file)) {
        Utils::print_and_log_msg "  $parent_file is newer than $file, recreating it ...";
        return 1;
    }

    return $positive_result;
}

sub die_r {
    my $msg = shift;
    my $add = scalar(@_) == 1? "\n". Dumper(shift) : "";
    die colored($msg.$add, "red on_white");
}


sub run_program {
    my ($name, $fastq_files, $outfile_basename, $options) = @_;
    my $is_paired = (scalar(@$fastq_files) == 2);
    make_path(dirname($outfile_basename)) unless -d dirname($outfile_basename);


    my $option_string = "";
    if (defined $options) {
        die_r("Options ref?? ".(ref $options)) unless ref $options eq 'HASH';
        $option_string = " ".join(", ", map { "$_: ".$options->{$_} } keys %$options)." ";
    }
    Utils::print_and_log_msg(colored($name,"black bold on_white")."$option_string", "magenta");
    foreach (@$fastq_files) { check_file($_, 0, "  Input:  "); }

    ( my $outfile_basename1 = $outfile_basename ) =~ s/%/1/;
    ( my $outfile_basename2 = $outfile_basename ) =~ s/%/2/;

    my $prog = get_conf($name);
    my $cmd1 = $prog->{'cmd'};
    my $bin = $prog->{'bin'};

    die_r("cmd for $name??",$prog) unless defined $cmd1;
    my @cmds = (ref $cmd1->[0] eq 'ARRAY'? @$cmd1 : ($cmd1));

    my $res_file;
    foreach my $cmd12 (@cmds) {
        die_r "ref cmd?? $cmd12" unless ref $cmd12 eq 'ARRAY';
        die_r "size cmd?? @$cmd12".Dumper($cmd12) unless scalar @$cmd12 == 2;
        my $cmd = $cmd12->[0];
	    $cmd =~ s/{bin}/$bin/g;
	    $cmd =~ s/{fastq-files}/@$fastq_files/g;
	    $cmd =~ s/{fastq-file1}/$fastq_files->[1]/g if $is_paired;
	    $cmd =~ s/{fastq-file2}/$fastq_files->[2]/g if $is_paired;
	    $cmd =~ s/{prog}/$name/g;
	    $cmd =~ s/{outfile-basename}/$outfile_basename/g;
	    $cmd =~ s/{outfile-basename1}/$outfile_basename1/g;
	    $cmd =~ s/{outfile-basename2}/$outfile_basename2/g if $is_paired;
	    $cmd =~ s/{threads}/$threads/g;
	
	#    if ($cmd =~ /{fasta-file}/) {
	#        (my $fasta_file = $fastq_files->[0]) =~ s/\.fa?s?t?q$/.fa/;
	#        if (!-f $fasta_file) {
	#            print_and_log_msg("Creating file $fasta_file.");
	#            if ($is_paired) {
	#                die "Make paired work!";
	#            } else {
	#                open (my $FQ, "<", $fastq_files->[0]) or die "$? $!";
	#                open (my $FA, ">", $fasta_file) or die "$? $!";
	#                while (<$FQ>) {
	#                    print ">", substr($_,1);
	#                    print <$FQ>;
	#                    <$FQ>; <$FQ>;
	#                }
	#                close($FQ);
	#                close($FA);
	#            }
	#        }
	#        $cmd =~ s/{fasta-file}/$fasta_file/g;
	#    }
	#
	    if ($is_paired) {
	        $cmd =~ s/{\?paired ([^}]*)}/$1/;
	    } else {
	        $cmd =~ s/{\?paired ([^}]*)}//;
	    }
	    if ($fastq_files->[0] =~ /.gz$/) {
	        $cmd =~ s/{\?gz ([^}]*)}/$1/;
	    } else {
	        $cmd =~ s/{\?gz ([^}]*)}//;
	    }
	
	    $options->{'extra-opts'} = "" unless defined $options->{'extra-opts'};
	    foreach my $opt (keys %$options) {
	        $cmd =~ s/{$opt}/$options->{$opt}/g;
	    }
	    die "Couldn't completely translate $cmd" if ($cmd =~ /[{}]/); 
	
	    if (file_isnt_ok($outfile_basename.$cmd12->[1])) {
	        system_call($cmd);
	    }
        $res_file = $outfile_basename.$cmd12->[1];
        check_file($res_file, 0, "  Result: ");
    }

    return($res_file)
}

sub get_pair {
    my ($name, $is_paired) = @_;
    if ($is_paired) {
        (my $name1 = $name) =~ s/%/1/;
        (my $name2 = $name) =~ s/%/2/;
        return($name1, $name2);
    } else {
        return $name;
    }
}

sub get_conf {
    my ($c) = @_;
    die "$c is not specified in the configuration (PipelineConf.pm)" if (!defined $conf{$c});
    return $conf{$c};
}


