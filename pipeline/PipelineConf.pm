package PipelineConf;

my %common_blast = (
    cmd => [
    [ "pv {fastq-files} | seqtk seq -A | {bin} -db {database} -task {prog} -outfmt '6 qseqid sseqid evalue bitscore pident length sgi sacc staxids sscinames scomnames stitle sstart ssend' -num_threads {threads} -max_hsps 1 -max_target_seqs 100 {extra-opts} > {outfile-basename}.m8", ".m8"],
    [ "cat {outfile-basename}.m8 | ~/projects/metagenomics-scripts/mglca/lca db/kraken/p-170210/taxDB | tee {outfile-basename}.e10.lca | ~/projects/metagenomics-scripts/mglca/report db/kraken/p-170210/taxDB > {outfile-basename}.e10.report", ".e10.report" ],
    [ "awk '\$3 < 0.01' {outfile-basename}.m8 | ~/projects/metagenomics-scripts/mglca/lca db/kraken/p-170210/taxDB | tee {outfile-basename}.e0.01.lca | ~/projects/metagenomics-scripts/mglca/report db/kraken/p-170210/taxDB > {outfile-basename}.e0.01.report", ".e0.01.report"],
    [ "awk '\$3 < 0.00001' {outfile-basename}.m8 | ~/projects/metagenomics-scripts/mglca/lca db/kraken/p-170210/taxDB | tee {outfile-basename}.e10m5.lca | ~/projects/metagenomics-scripts/mglca/report db/kraken/p-170210/taxDB > {outfile-basename}.e10m5.report", ".e10m5.report"]],
    extract_reads => {
        ext => ".lca",
        readid_column => 1,
        taxid_column => 2
    }
);
    
our %conf = (
    default => {
    	diamond => {
            dir => "sw/diamond",
            bin => "sw/diamond/diamond",
            cmd => [[ "{bin} blastx -d {database} -p {threads} -q {fastq-files} {extra-opts} -f 6 --top 5 -o {outfile-basename}.m8", ".m8"],
                    [ "join -1 2 -2 1 -o 1.1,2.2 -t \$'\\t' <(sort -k 2,2 {outfile-basename}.m8) <(gunzip -c {database}.seqid2taxid.map.gz) | sort -u | ~/projects/metagenomics-scripts/mglca/lca db/kraken/p-170210/taxDB -t 1 | tee {outfile-basename}.lca | ~/projects/metagenomics-scripts/mglca/report db/kraken/p-170210/taxDB > {outfile-basename}.report", ".report"]],
            install => {
                bin => "wget http://github.com/bbuchfink/diamond/releases/download/v0.8.36/diamond-linux64.tar.gz -O {dir}/diamond-linux64.tar.gz && tar xvvf {dir}/diamond-linux64.tar.gz -C {dir}",
                nr_database => "curl ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nr.gz | gunzip -c > {basename}-{timestamp}.faa && {bin} makedb --in {basename}-{timestamp}.faa -d {basename}-{timestamp}"
            }
        },
        spades => {
            bin => "bin/spades.py",
            install_cmd => "wget http://cab.spbu.ru/files/release{version}/SPAdes-{version}-Linux.tar.gz -O {dir}/SPAdes-{version}-Linux.tar.gz && tar xvvf {dir}/SPAdes-{version}-Linux.tar.gz -C {dir}",
            version => "3.10.0",
            cmd => "{bin} -o {outfile-basename} -s {fastq-files} -t {threads}"
        },
        
        blast_dir => "sw/blast",
        blastx => { bin => "blastx", %common_blast },
        blastn => { bin => "blastn", %common_blast },
        megablast => { bin => "blastn", %common_blast },
        "dc-megablast" => { bin => "blastn", %common_blast}, 

        kraken => {
            dir => "sw/kraken",
            bin => "sw/kraken/install/kraken",
            build_bin => "sw/kraken/install/kraken-build",
            extract_reads => {
                ext => ".gz",
                readid_column => 2,
                taxid_column => 3
            },
            extract_reads_script => "/home/fbreitwieser/projects/metagenomics-scripts/kraken/print-all-reads-matching-taxa.perl",
            cmd => ["{bin} --db {database} -t {threads} {?gz --gzip-compressed} {?paired --paired} --fastq-input {fastq-files} --report-file {outfile-basename}.report {extra-opts} --output {outfile-basename}.gz", ".gz"]
            #cmd => "{bin} --db {database} -t {threads} {extra-opts} {?gz --gzip-compressed} {?paired --paired} --fastq-input {fastq-files} | tee {outfile-basename}.kraken | {report-bin} --db {database} /dev/stdin > {outfile-basename}.report",
        },

        centrifuge => {
            dir => "sw/centrifuge",
            bin => "sw/centrifuge/centrifuge",
            build_bin => "sw/centrifuge/centrifuge-build",
    	    centrifuge_nt_database => "db/centrifuge/nt/nt"
        },

        kslam => {
            dir => "sw/kslam",
            bin => "sw/kslam/SLAM",
            build_script => "bash sw/kslam/install_slam_new_db.sh",
            cmd => "gunzip -c {fastq-files} | {bin} --db {database} --output-file={outfile-basename} --sam-file {outfile-basename}.sam /dev/stdin"
            #cmd => "{bin} --db {database} --output-file={outfile-basename} --sam-file {outfile-basename}.sam <(gunzip -c {fastq-files} )"
            #cmd => "{bin} --db {database} --output-file={outfile-basename} --sam-file >( samtools view -Su - | samtools sort -T {outfile-basename}-sort-tmp - -o {outfile-basename}.bam && samtools index -T {outfile-basename}-index-tmp {outfile-basename}.bam ) <(gunzip -c {fastq-files} )"
        },

        bbtools_dir => "sw/bbtools",
        bbduk_bin => "sw/bbtools/bbduk.sh",

        bbduk => {
            bin => "sw/bbtools/bbduk.sh",
            cmd => "{bin} in={fastq-file1} out={outfile-basename1} threads={threads} ref=sw/bbtools/resources/adapters.fa"
                    ." qtrim=rk trimq=25 qtrim=rl entropy=0.5 minavgquality=20"
                    ." {?paired in2=fastq-file2} out2={outfile2}"
                    ." 2>&1 | tee {outfile-basename}.q_trimmed.fq.log "

        },

	    hs_bowtie2_idx => undef,
    	hs_hisat2_idx => undef,
	    mm_bowtie2_idx => undef,
    	decoy1k_bowtie2_idx => undef,
	    rrna_bowtie2_idx => undef,
    	kallisto_idx => undef,
    #	bam_params => undef,
    	sam_to_fastq_cmd => undef,
    	scripts_dir => undef,
    	idba_dir => undef,
    	mpa_dir => undef,
    	metaphlan_bin => undef,
    	mtools_dir => undef,
    	kraken_report_script => undef,
    	kraken_report_xlsx => undef,
    	krona_install_dir => undef
    },
    igm3 => {
	    kraken_db => "/scratch0/igm3/data/krakendbs/current",
    	centrifuge_db => "/scratch0/igm3/data/centrifugedbs/nt-dusted/nt",
	    hs_bowtie2_idx => "/scratch0/igm3/data/indices/GRCh38.p2_bowtie2/hs_ref_GRCh38.p2",
    	hs_hisat2_idx => "/scratch0/igm3/data/indices/grch38_snp_tran/genome_snp_tran",
	    mm_bowtie2_idx => "/scratch0/igm3/data/indices/mm10_bowtie2/mm10",
    	decoy1k_bowtie2_idx => "/scratch0/igm3/data/indices/human_decoy1k",
	    rrna_bowtie2_idx => "/scratch0/igm3/data/indices/silva_bowtie2/silva_bowtie2",
    	kallisto_idx => "/scratch0/igm3/data/krakendbs/kallisto-db/i100_augmented.idx",
    #	bam_params => "--local --very-fensitive-local --mm --threads $threads",
    	sam_to_fastq_cmd => 'bam2fastx -f /dev/stdin',
    	scripts_dir => '/home/fbreitwieser/projects/microbiomics-scripts',
    	idba_dir => "/scratch0/igm3/fbreitwieser/test-microbiomics-sw/idba-1.1.0",
    	diamond_bin => "/scratch0/igm3/fbreitwieser/test-microbiomics-sw/diamond/bin/diamond",
    	centrifuge_bin => "/home/fbreitwieser/projects/centrifuge-master/centrifuge",
    	diamond_nr_database => "/scratch0/igm3/fbreitwieser/test-microbiomics-sw/diamond/nr",
    	mpa_dir => "/scratch0/igm3/fbreitwieser/test-microbiomics-sw/metaphlan-2.2.0",
    	metaphlan_bin => "/scratch0/igm3/fbreitwieser/test-microbiomics-sw/metaphlan-2.2.0/metaphlan2.py",
    	mtools_dir => "/scratch0/igm3/fbreitwieser/test-microbiomics-sw/mtools",
    	kraken_report_script => "/home/fbreitwieser/projects/microbiomics-scripts/kraken/kraken-report-depth",
    	kraken_report_xlsx => "/home/fbreitwieser/projects/microbiomics-scripts/kraken/kraken-report-xlsx.py",
    	krona_install_dir => "/scratch0/igm3/fbreitwieser/test-microbiomics-sw/KronaTools-2.6"
    },
    lion => {
	    kraken_db => undef,
    	centrifuge_db => undef,
	    hs_bowtie2_idx => undef,
    	hs_hisat2_idx => undef,
	    mm_bowtie2_idx => undef,
    	decoy1k_bowtie2_idx => undef,
	    rrna_bowtie2_idx => undef,
    	kallisto_idx => undef,
    #	bam_params => undef,
    	sam_to_fastq_cmd => undef,
    	scripts_dir => undef,
    	idba_dir => undef,
    	diamond_bin => undef,
    	centrifuge_bin => undef,
    	diamond_nr_database => undef,
    	mpa_dir => undef,
    	metaphlan_bin => undef,
    	mtools_dir => undef,
    	kraken_report_script => undef,
    	kraken_report_xlsx => undef,
    	krona_install_dir => undef
    }
);

1;
