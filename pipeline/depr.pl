    

    if (get_option("filter")) {
        -d "$my_processing_dir/align" || `mkdir -vp $my_processing_dir/align`;
	    ## TODO: Should I use hisat / tophat2 to remove spliced reads, or is bowtie --local sensitive enough?
	    ## filter human reads from FASTQ file
	    $new_fastq_file = filter_w_bowtie(
	        $new_fastq_file,
	        "$my_processing_dir/fastq/$basename$postfix.filtered1_no-hs.fastq",
	        "$my_processing_dir/align/$basename.hs.bam",
	        get_conf("hs_bowtie2_idx"),
	        $is_paired
	    );
	
	    ## filter mouse reads from FASTQ file
	    $new_fastq_file = filter_w_bowtie(
	        $new_fastq_file,
	        "$my_processing_dir/fastq/$basename$postfix.filtered2_no-hs_mm.fastq",
	        "$my_processing_dir/align/$basename.mm.bam",
	        get_conf("mm_bowtie2_idx"),
	        $is_paired);

    }


    if (get_option("hisat2")) {
        -d "$my_processing_dir/align" || `mkdir -vp $my_processing_dir/align`;
	    filter_w_hisat(
	        $new_fastq_file,
	        "$my_processing_dir/fastq/$basename$postfix.filtered1_no-hs.fastq",
	        "$my_processing_dir/align/$basename.hisat2.bam",
	        get_conf("hs_hisat2_idx"),
	        $is_paired
	    );
    }

	    ## filter rRNA and SILVA vector sequences
#	    if ($is_rnaseq) {
#	        $new_fastq_file = run_sortmerna(
#	            $new_fastq_file,
#	            "$my_processing_dir/fastq/$basename$postfix.no_rrna",
#	            "$my_processing_dir/fastq/$basename$postfix.rrna",
#	            $rrna_bowtie2_idx,
#	            $is_paired);
#	    } 


    if (get_option('extended-kraken-report')) {
        $kraken_report = "$my_processing_dir/report/$basename_m$name_suffix.report_extended";
    }


    my $kraken_db = get_option_or_default("use-kraken-db", get_conf("kraken_db"));
    my $name_suffix .= ".".basename($kraken_db);

    my $kraken0_fastq = "$my_processing_dir/fastq/$basename.unidentified.fastq.gz";
    if (get_option("against-unidentified")) {
        $name_suffix .= ".against0";
        ## TODO: Works only for paired end, currently
        @files = (get_p($kraken0_fastq,1), get_p($kraken0_fastq,2));
        if (!-s get_p($kraken0_fastq,2)) {
            print STDERR "$files[0] does not exist!\n";
            next;
        }
    }

    my $kraken_file = "$my_processing_dir/kraken/$basename_m$name_suffix.kraken";
    my $krakengz_file = "$my_processing_dir/kraken/$basename_m$name_suffix.kraken.gz";
    my $kraken_report = "$my_processing_dir/report/$basename_m$name_suffix.report";
    if (get_option('extended-kraken-report')) {
        $kraken_report = "$my_processing_dir/report/$basename_m$name_suffix.report_extended";
    }
    -d "$my_processing_dir/kraken" || `mkdir -vp $my_processing_dir/kraken`;
    if (file_isnt_ok($kraken_file,undef,'force-kraken')) {
        system_call("kraken --db $kraken_db -t $threads".
                    ($is_paired? " --paired" : "").
                    ($files[0] =~ /.gz/? " --gzip-compressed" : "").
                    " --fastq-input @files".
                    " | kraken-host /dev/stdin > $kraken_file");
    } else {
        print " kraken$checkmark"; $|++;
    }
    -d "$my_processing_dir/report" || `mkdir -vp $my_processing_dir/report`;
    if (file_isnt_ok($kraken_report,$kraken_file,'force-kraken-report')) {
        print STDERR "\n";
        #if (stat($kraken_file)->mtime > stat($kraken_report)->mtime) {
        ## create a kraken report
        if (!get_option('extended-kraken-report')) {

            system_call(get_conf("kraken_report_script"),
                    " --db=$kraken_db $kraken_file > $kraken_report");
            #system_call("$kraken_report_xlsx $kraken_report");
            } else {
            system_call("kraken-report-modif ".
                    " --db=$kraken_db $kraken_file".
                    ($is_paired? " --paired" : "").
                    " $new_fastq_file > $kraken_report");
            next;
            }
    } else {
        print " report$checkmark"; $|++;
    }
    

    ###########################################################################
    ## extract reads w/ specified taxid
    my @extract_reads_files;
    if (option_defined("extract-reads")) {
        for my $taxid (split(/,/,get_option("extract-reads"))) {
        print STDERR "extract-reads\n";
        #my ($res) = system_exec("grep \"[^\t]*\t[^\t]*\t[^\t]*\t[^\t]*\t$taxid\t\" $kraken_report");
        my ($res) = `grep \"\t$taxid\t\" $kraken_report`;
        next unless defined $res;
        chomp $res;
        my @res = split(/\t/,$res);
        my $name;
        if (defined $res[$#res]) {
            $name = $res[$#res];
            $name =~ s/^ *//g;
            $name =~ s/[^A-Za-z0-9\-\.]/_/g;

        } else {
            print STDERR "No res\n";
        }
            #print STDERR "$res[9]\n";
           	        my $extract_reads_file = "$my_processing_dir/fastq/$basename.$name.$taxid.fq";
            push @extract_reads_files, $extract_reads_file; 

            if (file_isnt_ok(get_p($extract_reads_file,2),undef,undef)) {
            
            foreach my $file_p (@file_ps) {
                system_exec(
	                "print-all-reads-matching-taxa.perl".
	                " ".$taxid.
	                " ".$kraken_file.
                    " ".get_p($new_fastq_file,$file_p)," > ".get_p($extract_reads_file,$file_p));
	        }
            }
        }
    }

	
	        if (get_option("check-files")) {
		        ## check if file is open
                #if (system("list-my-open-files | grep `pwd`/$fastq_file") == 0) {
		            #file found in the list - skipping it
                    #    print "list-my-open-files | grep `pwd`/$fastq_file was succesful - skipping the file for now\n";
                    #} 
		
		        ## check file sizes
		        my $file_size = (stat($fastq_file))[7];
		        if (!defined $file_sizes{$fastq_file}) {
		            $file_sizes{$fastq_file} = $file_size;
		            print "+"; $|++; next;
                }
                
                if ($file_sizes{$fastq_file} ne $file_size) {
		            $file_sizes{$fastq_file} = $file_size;
		            print "~"; $|++; next;
		        }
	        }
	


    ###########################################################################
    ## extract non-human reads
  
if (option_defined("make-fasta")) {
    if (!-s get_p($unidentified_reads_fa,2) || get_option("force")) {
      foreach my $file_p (@file_ps) {
        system_call("$fastq_to_fasta ".get_p($kraken0_fastq,$file_p).
            "> ".get_p($unidentified_reads_fa,$file_p));
      }
    }
    }


    #my $kraken0_fastq = "$my_processing_dir/fastq/$basename.unidentified.fastq";
    if (get_option("extract-nonhuman-reads")) {
    }

    my $filter_taxid = "9606,32630,81077";
    my $nohuman_reads_fq = "$my_processing_dir/fastq/$basename.nohuman.fastq.gz";
    my $nohuman_reads_fq_filtered = "$my_processing_dir/fastq/$basename.nohuman.q_filtered.fastq.gz";
    my $nohuman_reads_fa = "$my_processing_dir/fastq/$basename.nohuman.q_filtered.fasta";
    my $nohuman_reads_fa_np = "$my_processing_dir/fastq/$basename_m.nohuman.q_filtered.fasta";
 
    if (!-s get_p($nohuman_reads_fq,2) || get_option("force")) {
        ## extract nohuman reads
        my $cmd = "print-all-reads-matching-taxa.perl -i $filter_taxid $kraken_file";
        foreach my $file_p (@file_ps) {
            system_exec($cmd." ".get_p($new_fastq_file,$file_p)," | gzip -c > ".get_p($nohuman_reads_fq,$file_p));
        }
    }

    if (option_defined("q-filter")) {
    if (!-s get_p($nohuman_reads_fq_filtered,2) || get_option("force")) {
        ## extract nohuman reads
        foreach my $file_p (@file_ps) {
            my $cmd = sprintf("gunzip -c %s | /scratch0/igm3/fbreitwieser/test-microbiomics-sw/bbmap/bbduk.sh in=stdin.fq out=stdout.fq qtrim=rl trimq=5 entropy=0.9 | gzip -c > %s 2> %s.log",
                              get_p($nohuman_reads_fq,$file_p),get_p($nohuman_reads_fq_filtered,$file_p),get_p($nohuman_reads_fq_filtered,$file_p));
            system_exec($cmd);
        }
    }
}



    if (option_defined("make-fasta")) {
    if (!-s get_p($nohuman_reads_fa,2) || get_option("force")) {
      foreach my $file_p (@file_ps) {
        system_call("$fastq_to_fasta ".get_p($nohuman_reads_fq_filtered,$file_p).
            " > ".get_p($nohuman_reads_fa,$file_p));
      }
    }
}



if (0) {
    -d "$my_processing_dir/krona" || `mkdir -vp $my_processing_dir/krona`;
    my $krona = "$my_processing_dir/krona/$basename_m.krona";
    my $krona_no0 = "$my_processing_dir/krona/$basename_m.krona_no0";
    my $krona_args = "-i $kraken_file -k $kraken_db -d $threads -p $krona_install_dir";
    if (check_file_ok($krona_install_dir,$kraken_file,"force")) {
        print STDERR "\n";
        system_call("$scripts_dir/kraken2krona.sh $krona_args -u -o $krona_no0.txt -r $krona_no0.html");
        system_call("$scripts_dir/kraken2krona.sh $krona_args -o $krona.txt -r $krona.html");
    } else {
        print " krona$checkmark"; $|++;
    }
    }

 #my $filter_microbial_taxid = "9606,32630,81077,0,1";
    my $microbial_names = "d_Bacteria k_Fungi d_Viruses";
    my $microbial_reads_fa = "$my_processing_dir/fastq/$basename.microbial.fasta";
    my $microbial_reads_fq = "$my_processing_dir/fastq/$basename.microbial.fastq";
    my $microbial_reads_fa_np = "$my_processing_dir/fastq/$basename_m.microbial.fasta";
 
    if (0) {
    my $microbial_taxid = `Rscript get-taxids.R $kraken_report $microbial_names`; chomp $microbial_taxid;
    if (!-s get_p($microbial_reads_fq,2) || get_option("force")) {
        ## extract microbial reads
        my $cmd = "print-all-reads-matching-taxa.perl $microbial_taxid $kraken_file";
        foreach my $file_p (@file_ps) {
            system_exec($cmd." ".get_p($new_fastq_file,$file_p)," > ".get_p($microbial_reads_fq,$file_p));
        }
    }

    if (!-s get_p($microbial_reads_fa,2) || get_option("force")) {
      foreach my $file_p (@file_ps) {
        system_call("$fastq_to_fasta ".get_p($microbial_reads_fq,$file_p).
            " > ".get_p($microbial_reads_fa,$file_p));
      }
    }
    }

    ###########################################################################
    ## align to a specified genome
    if (option_defined("align-to")) {
        my $ref_genome = get_option("align-to");
        if (!-f $ref_genome.".1.bt2") {
            print STDERR "Should I create a bowtie2 index using the files ".$ref_genome."/*.fna? [Yn]    ";
            my $answer = <STDIN>; chomp $answer;
            if ($answer eq "" || $answer =~ /^[yY]/) {
                my $reference_in = `ls $ref_genome/*.{fna,fa,fasta} | paste -sd,`;
                chomp $reference_in;
                system_call("bowtie2-build $reference_in ".$ref_genome);
            } else {
                next;
            }
        }
        #my $search_file = $kraken0_fastq;
        #my $search_file = $kraken0_fastq;
        my $search_file = $nohuman_reads_fq;
        #my $search_file = $nohuman_reads_fq_filtered;
        #my $search_file = $new_fastq_file;
        if (!option_defined("extract-reads")) {
          print STDERR "Use option extract-reads when aligning to a genome to specify reads." ;
        } else {
          $search_file = join(",",@extract_reads_files);
        }
        -d "$my_processing_dir/align" || `mkdir -vp $my_processing_dir/align`;
        my $fname="$my_processing_dir/align/$basename_m.".basename(dirname($ref_genome)).".bam";
        my $fname_rmdup="$my_processing_dir/align/$basename_m.".basename(dirname($ref_genome))."-rmdup.bam";
	    filter_w_bowtie(
                $search_file,
	            undef,
                $fname,
	            $ref_genome,
	            $is_paired, "-L 31");
        system_call("samtools rmdup $fname $fname_rmdup");
    }

    ###########################################################################
    ## run METAPHLAN
    -d "$my_processing_dir/metaphlan" || `mkdir -vp $my_processing_dir/metaphlan`;
    my $metaphlan_file = "$my_processing_dir/metaphlan/$basename_m.metaphlan.txt";
    my $metaphlan_bt = "$my_processing_dir/metaphlan/$basename_m.metaphlan.bowtie.bz2";
    if (!-s $metaphlan_file || get_option("force")) {
        my $metaphlan_input_file = $nohuman_reads_fq;
        if ($is_paired) {
            $metaphlan_input_file = get_p($nohuman_reads_fq,1).",".get_p($nohuman_reads_fq,2);
        }
        system_call("time",get_conf("metaphlan_bin"),"$metaphlan_input_file --nproc $threads --mpa_pkl ".get_conf("mpa_dir")."/db_v20/mpa_v20_m200.pkl".
                    " --bowtie2db ".get_conf("mpa_dir")."/db_v20/mpa_v20_m200 --bowtie2out $metaphlan_bt ".
                    "--bt2_ps sensitive-local --min_alignment_len 51 --input_type fastq >  $metaphlan_file");
    } else {
        print " metaphlan$checkmark"; $|++;
    }

    ###########################################################################
    ## run kallisto

    if (get_option("kallisto")) {
        -d "$my_processing_dir/align" || `mkdir -vp $my_processing_dir/align`;
        system_call("time kallisto quant -i ",get_conf("kallisto_idx")," --plaintext".
            ($is_paired? "" : " --single").
            " -l 200 -s 20 -t $threads".
            " -o $my_processing_dir/align $nohuman_reads_fq");
    }




    ## run the idba assembler on the nohuman reads
    my $idba_result_dir = "$my_processing_dir/assemble-$basename";
    ## TODO
    
    ###########################################################################
    ## run DIAMOND on non-human reads
    if (get_option("blastx"))  {
    -d "$my_processing_dir/blastx" || `mkdir -vp $my_processing_dir/blastx`;
    if ($is_paired) {
        if (!-f $nohuman_reads_fa_np) {
            system_call("paste -d'\\0' ".get_p($nohuman_reads_fa,1)." ".get_p($nohuman_reads_fa,2)." > $nohuman_reads_fa_np");
        }
        if (!-f $unidentified_reads_fa_np) {
            system_call("paste -d'\\0' ".get_p($unidentified_reads_fa,1)." ".get_p($unidentified_reads_fa,2)." > $unidentified_reads_fa_np");
        }
    } else {
        if (!-f $nohuman_reads_fa_np) {
            system_call("gunzip -c $nohuman_reads_fq_filtered | fq2fa > $nohuman_reads_fa_np");
        }

    }
    my $diamond_res = "$my_processing_dir/blastx/$basename_m";
    if (!-s "$diamond_res.daa" || get_option("force")) {
        system_call("time",get_conf("diamond_bin")," blastx -d ",get_conf("diamond_nr_database")," -q $nohuman_reads_fa_np -a $diamond_res -p $threads");
        system_call("time",get_conf("diamond_bin")," view -a $diamond_res.daa -o $diamond_res.m8");
    } else {
        print " diamond$checkmark"; $|++;
    }

    ## run mtools
    my $mtools_res = "$my_processing_dir/blastx/$basename_m.lca";
    if ((!-s "$mtools_res" || get_option("force")) && -f "$diamond_res.m8") {
        #system_call("time $mtools_dir/bin/lcamapper.sh -i $diamond_res.m8 -f Detect -ms 50 -me 0.01 -tp 50 -gt $mtools_dir/data/gi_taxid_prot.bin -o $mtools_res");
        system_call("time",get_conf("mtools_dir")."/bin/lcamapper.sh -i $diamond_res.m8 -f Detect -ms 70 -me 0.001 -tp 70 -gt ",get_conf("mtools_dir")."/data/gi_taxid_prot.bin -o $mtools_res");
    } else {
        print " lca$checkmark"; $|++;
    }

    }

    ###########################################################################
    ## run CENTRIFUGE
    if (get_option("centrifuge")) {
    my $centrifuge_file = "$my_processing_dir/centrifuge/$basename_m.centrifuge.out";
    my $centrifuge_file2 = "$my_processing_dir/centrifuge/$basename_m.centrifuge.out.gz";
    my $centrifuge_report = "$my_processing_dir/centrifuge/$basename_m.centrifuge.report";
    my $centrifuge_report2 = "$my_processing_dir/centrifuge/$basename_m.centrifuge2.report";
    -d "$my_processing_dir/centrifuge" || `mkdir -vp $my_processing_dir/centrifuge`;

    if (file_isnt_ok($centrifuge_file2,undef,'force-centrifuge')) {
      #my $input_fastq = $new_fastq_file;
      my $input_fastq = $nohuman_reads_fq_filtered;
      my $cf_opts;
      if ($is_paired) {
          (my $input_fastq1 = $input_fastq) =~ s/%/1/g or die "$input_fastq??";
          (my $input_fastq2 = $input_fastq) =~ s/%/2/g or die "$input_fastq??";
          $cf_opts = "-1 $input_fastq1 -2 $input_fastq2";
      } else {
          $cf_opts = "-U $input_fastq";
      }

      system_call("time",get_conf("centrifuge_bin"),"--mm -x ",get_conf("centrifuge_db")," -p $threads -k 1 ".
                    " --report-file $centrifuge_report --host-taxids 9606,32630".
                    " --tab-fmt-cols readID,seqID,taxID,score,2ndBestScore,hitLength,queryLength,numMatches,readSeq".
                    " $cf_opts | tee $centrifuge_file | awk '\$3 != 9606 && \$3 != 32630' | sed 1d | sort -t \$'\t' -k3,3 -k6,6n | bgzip -c > $centrifuge_file2");
      system_call("time tabix -s 3 -b 6 -e 6 $centrifuge_file2");
    }

    if (file_isnt_ok($centrifuge_report2,undef,'force-centrifuge')) {
      system_call("time ~/projects/centrifuge/centrifuge-kreport -x ",get_conf("centrifuge_db")," --min-length=50 $centrifuge_file".
                    " > $centrifuge_report2");
    }
    print " centrifuge$checkmark"; $|++;
    }

    if (get_option("centrifuge2")) {
    my $centrifuge_file = "$my_processing_dir/centrifuge2/$basename_m.centrifuge.out";
    my $centrifuge_file2 = "$my_processing_dir/centrifuge2/$basename_m.centrifuge.out.gz";
    my $centrifuge_report = "$my_processing_dir/centrifuge2/$basename_m.centrifuge.report";
    -d "$my_processing_dir/centrifuge2" || `mkdir -vp $my_processing_dir/centrifuge2`;

    my $centrifuge_bin = "~/projects/centrifuge/centrifuge";
    if (file_isnt_ok($centrifuge_file2,undef,'force-centrifuge')) {
      #my $input_fastq = $new_fastq_file;
      my $input_fastq = $nohuman_reads_fq_filtered;
      my $cf_opts;
      if ($is_paired) {
          (my $input_fastq1 = $input_fastq) =~ s/%/1/g or die "$input_fastq??";
          (my $input_fastq2 = $input_fastq) =~ s/%/2/g or die "$input_fastq??";
          $cf_opts = "-1 $input_fastq1 -2 $input_fastq2";
      } else {
          $cf_opts = "-U $input_fastq";
      }

      system_call("time $centrifuge_bin --mm -x ",get_conf("centrifuge_db")," -p $threads -k 1 ".
                    " --report-file $centrifuge_report --host-taxids 9606,32630 --min-totallen 50".
                    " $cf_opts | bgzip -c > $centrifuge_file2");
        #system_call("time tabix -s 3 -b 6 -e 6 $centrifuge_file2");
    }

    print " centrifuge2$checkmark"; $|++;
    }

    ###########################################################################
    ## run HUMANN
    if (get_option("humann2")) {
    my $humann2_outdir = "$my_processing_dir/humann2";
    -d "$my_processing_dir/humann2" || `mkdir -vp $my_processing_dir/humann2`;

    my $humann2_bin = "humann2";
    if (file_isnt_ok($humann2_outdir,undef,'force-humann2')) {
      my $input_fastq = $new_fastq_file;
      my $cf_opts;
      if ($is_paired) {
          (my $input_fastq1 = $input_fastq) =~ s/%/1/g or die "$input_fastq??";
          (my $input_fastq2 = $input_fastq) =~ s/%/2/g or die "$input_fastq??";
          #$cf_opts = "-1 $input_fastq1 -2 $input_fastq2";
          die "humann2 does not work with paired files";
      } else {
          $cf_opts = "--input $nohuman_reads_fq";
      }

      system_call("time $humann2_bin $cf_opts --output $humann2_outdir");
    } else {
        print " humann2$checkmark"; $|++;
    }
    }


sub run_sortmerna {
    my ($input_fastq,$filtered_output_fastq,$rrna_output_fastq,$is_paired) = @_;

    my $idx="/home/fbreitwieser/src/sortmerna/rRNA_databases/rfam-5.8s-database-id98.fasta,/home/fbreitwieser/src/sortmerna/index/rfam-5.8s-database-id98:/home/fbreitwieser/src/sortmerna/rRNA_databases/rfam-5s-database-id98.fasta,/home/fbreitwieser/src/sortmerna/index/rfam-5s-database-id98:/home/fbreitwieser/src/sortmerna/rRNA_databases/silva-arc-16s-id95.fasta,/home/fbreitwieser/src/sortmerna/index/silva-arc-16s-id95:/home/fbreitwieser/src/sortmerna/rRNA_databases/silva-arc-23s-id98.fasta,/home/fbreitwieser/src/sortmerna/index/silva-arc-23s-id98:/home/fbreitwieser/src/sortmerna/rRNA_databases/silva-bac-16s-id90.fasta,/home/fbreitwieser/src/sortmerna/index/silva-bac-16s-id90:/home/fbreitwieser/src/sortmerna/rRNA_databases/silva-bac-23s-id98.fasta,/home/fbreitwieser/src/sortmerna/index/silva-bac-23s-id98:/home/fbreitwieser/src/sortmerna/rRNA_databases/silva-euk-18s-id95.fasta,/home/fbreitwieser/src/sortmerna/index/silva-euk-18s-id95:/home/fbreitwieser/src/sortmerna/rRNA_databases/silva-euk-28s-id98.fasta,/home/fbreitwieser/src/sortmerna/index/silva-euk-28s-id98 --reads tumor_R2.trimmed.fq.gz.fq";

    my $opts = "-a $threads --num_alignments 1 --fastx --log --ref $idx";
    if ($is_paired) {
        (my $input_fastq1 = $input_fastq) =~ s/%/1/ or die "$input_fastq??";
        (my $filtered_output_fastq1 = $filtered_output_fastq) =~ s/%/1/ or die "$filtered_output_fastq??";
        (my $rrna_output_fastq1 = $rrna_output_fastq) =~ s/%/1/ or die "$rrna_output_fastq??";

        if (!-s $filtered_output_fastq1.".fq" || get_option("force")) {
        system_call("sortmerna $opts --reads $input_fastq1 --other $filtered_output_fastq1 --aligned $rrna_output_fastq1");
        }

        (my $input_fastq2 = $input_fastq) =~ s/%/2/ or die "$input_fastq??";
        (my $filtered_output_fastq2 = $filtered_output_fastq) =~ s/%/2/ or die "$filtered_output_fastq??";
        (my $rrna_output_fastq2 = $rrna_output_fastq) =~ s/%/2/ or die "$rrna_output_fastq??";
        if (!-s $filtered_output_fastq1.".fq" || get_option("force")) {
        system_call("sortmerna $opts --reads $input_fastq2 --other $filtered_output_fastq2 --aligned $rrna_output_fastq2");
        }

    } else {
        if (!-s $filtered_output_fastq.".fq" || get_option("force")) {
        system_call("sortmerna $opts --reads $input_fastq --other $filtered_output_fastq --aligned $rrna_output_fastq");
        }

    }


    return $filtered_output_fastq.".fq";
}

sub filter_kraken {
    my ($fastq_files, $kraken_file, $taxid, $result_fq_taxid) = @_;
    my $is_paired = (scalar(@$fastq_files) == 2);

    make_path(dirname($result_fq_taxid)) unless -d dirname($result_fq_taxid);

    my @result_files = get_pair($result_fq_taxid.".fq.gz", $is_paired);
    if (!-s "$result_files[0]") {
        system_call(get_conf("extract_kraken_reads_script")." $taxid $kraken_file $fastq_files->[0] | gzip -c > $result_files[0]");
        system_call(get_conf("extract_kraken_reads_script")." $taxid $kraken_file $fastq_files->[1] | gzip -c > $result_files[1]") if $is_paired;
    }

    my @trimmed_result_files = get_pair("$result_fq_taxid.q_trimmed.fq.gz", $is_paired);
    if (!-s "$trimmed_result_files[0]") {
        my $cmd = get_conf("bbduk_bin")." in=$result_files[0] out=$trimmed_result_files[0]"
                    ." threads=$threads ref=".get_conf("bbtools_dir")."/resources/adapters.fa"
                    ." qtrim=rk trimq=25 qtrim=rl trimq=5 entropy=0.5 minavgquality=20";
        $cmd .= " in2=$result_files[1] out2=$trimmed_result_files[1]" if $is_paired;
        $cmd .= " 2>&1 | tee $result_fq_taxid.q_trimmed.fq.log ";
        system_call($cmd);
    }

    Utils::print_and_log_msg("  $trimmed_result_files[0] $checkmark");
    return (@trimmed_result_files);
}

sub run_kraken {
    my ($database, $fastq_files, $outfile_basename, $extra_opts) = @_;
    make_path(dirname($outfile_basename)) unless -d dirname($outfile_basename);

    my $is_paired = (scalar(@$fastq_files) == 2);
    my $kraken_file = "$outfile_basename.kraken";
    #my $krakengz_file = "$outfile_basename.kraken.gz";
    my $kraken_report = "$outfile_basename.report";

    if (file_isnt_ok($kraken_report)) {
        my $cmd = (get_conf("kraken")->{"bin"})." --db $database -t $threads"
                        .(defined $extra_opts? " $extra_opts" : "")
                        .($is_paired? " --paired" : "")
                        .($fastq_files->[0] =~ /.gz/? " --gzip-compressed" : "")
                        ." --fastq-input @$fastq_files | tee $kraken_file"
                        ." | ".(get_conf("kraken")->{"report_bin"})
                        ." --db=$database /dev/stdin > $kraken_report";
        system_call($cmd);
    }

    Utils::print_and_log_msg("  $kraken_report $checkmark");
    return($kraken_file);
}

sub run_kraken2 {
    my ($sample_dir, $krakendb, $fastq_files, $name, $name2, $extract_unidentified_reads) = @_;

    my $is_paired = (scalar(@$fastq_files) == 2);
    my $kraken_file = "$sample_dir/kraken/$name.kraken";
    my $krakengz_file = "$sample_dir/kraken/$name.kraken.gz";

    my $kraken_report = "$sample_dir/kraken/$name.report";
    -d "$sample_dir/kraken" || `mkdir -vp $sample_dir/kraken`;
    -d "$sample_dir/fastq" || `mkdir -vp $sample_dir/fastq`;

    if (file_isnt_ok($kraken_report)) {
        my $cmd = (get_conf("kraken")->{"bin"})." --db $krakendb -t $threads --quick"
                        .($is_paired? " --paired" : "")
                        .($fastq_files->[0] =~ /.gz/? " --gzip-compressed" : "")
                        ." --fastq-input @$fastq_files | tee $kraken_file"
                        ." | ".(get_conf("kraken")->{"report_bin"})
                        ." --db=$krakendb /dev/stdin > $kraken_report";
        system_call($cmd);
    }

    if ($extract_unidentified_reads) {
        die "paired reads currently not supported (fix Kraken!!)" if $is_paired;
        my $unidentified_reads_fq = "$sample_dir/fastq/$name2.unidentified.fq.gz";
        my @result_files = get_pair($unidentified_reads_fq, $is_paired);

        if (!-s "$result_files[0]") {
            system_call(get_conf("extract_kraken_reads_script")." 0 $kraken_file $fastq_files->[0] | gzip -c > $result_files[0]");
            system_call(get_conf("extract_kraken_reads_script")." 0 $kraken_file $fastq_files->[1] | gzip -c > $result_files[1]") if $is_paired;
        }

        my $trimmed_reads_fq = "$sample_dir/fastq/$name2.unidentified.q_trimmed.fq.gz";
        my @trimmed_result_files = get_pair($trimmed_reads_fq, $is_paired);

        if (!-s "$trimmed_result_files[0]") {
            my $cmd = get_conf("bbduk_bin")." in=$result_files[0] out=$trimmed_result_files[0]"
                        ." threads=$threads ref=".get_conf("bbtools_dir")."/resources/adapters.fa"
                        ." qtrim=rk trimq=25 qtrim=rl trimq=5 entropy=0.5 minavgquality=20";
            $cmd .= " in2=$result_files[1] out2=$trimmed_result_files[1]" if $is_paired;
            $cmd .= " 2>&1 | tee ";
            system_call($cmd);
        }
    }

    print " $kraken_report $checkmark";
}

sub filter_w_bowtie {
    return run_bt_hisat("bowtie2", "--local", @_);
}

sub filter_w_hisat {
    return run_bt_hisat("/scratch0/igm3/fbreitwieser/test-microbiomics-sw/hisat2/hisat2", "", @_);
}

sub run_bt_hisat {
    my ($prog, $params, $input_fastq, $filtered_output_fastq, $sam_file, $bowtie2_idx, $is_paired, $bt2_opts) = @_;

    if ($is_paired) {
        (my $input_fastq1 = $input_fastq) =~ s/%/1/g or die "$input_fastq??";
        (my $input_fastq2 = $input_fastq) =~ s/%/2/g or die "$input_fastq??";
        $bt2_opts .= " -1 $input_fastq1 -2 $input_fastq2";
        $bt2_opts .= " --un-conc '$filtered_output_fastq'" if defined $filtered_output_fastq;
    } else {
        $bt2_opts .= " -U $input_fastq";
        $bt2_opts .= " --un '$filtered_output_fastq'" if defined $filtered_output_fastq;
    }

    if (!-s $sam_file || get_option("force")) {
        system_call("$prog $params $bam_params -x $bowtie2_idx $bt2_opts | sam2sortedbam $sam_file");
    }
    return $filtered_output_fastq;
}



sub filter_kraken {
    my ($fastq_files, $kraken_file, $taxid, $result_fq_taxid) = @_;
    my $is_paired = (scalar(@$fastq_files) == 2);

    make_path(dirname($result_fq_taxid)) unless -d dirname($result_fq_taxid);

    my @result_files = get_pair($result_fq_taxid.".fq.gz", $is_paired);
    if (!-s "$result_files[0]") {
        system_call(get_conf("extract_kraken_reads_script")." $taxid $kraken_file $fastq_files->[0] | gzip -c > $result_files[0]");
        system_call(get_conf("extract_kraken_reads_script")." $taxid $kraken_file $fastq_files->[1] | gzip -c > $result_files[1]") if $is_paired;
    }

    my @trimmed_result_files = get_pair("$result_fq_taxid.q_trimmed.fq.gz", $is_paired);
    if (!-s "$trimmed_result_files[0]") {
        my $cmd = get_conf("bbduk_bin")." in=$result_files[0] out=$trimmed_result_files[0]"
                    ." threads=$threads ref=".get_conf("bbtools_dir")."/resources/adapters.fa"
                    ." qtrim=rk trimq=25 qtrim=rl trimq=5 entropy=0.5 minavgquality=20";
        $cmd .= " in2=$result_files[1] out2=$trimmed_result_files[1]" if $is_paired;
        $cmd .= " 2>&1 | tee $result_fq_taxid.q_trimmed.fq.log ";
        system_call($cmd);
    }

    check_file($trimmed_result_files[0], 0, "Filtered result: ");
    return (@trimmed_result_files);
}

sub extract_reads {
    my ($prog, $fastq_files, $res_file, $taxid, $result_fastq_basename) = @_;

    my $script = get_conf($prog)->{"extract_reads_script"};
    die "No extract reads script for prog" unless defined $script;
    my $is_paired = (scalar(@$fastq_files) == 2);

    make_path(dirname($result_fastq_basename)) unless -d dirname($result_fastq_basename);

    my @result_files = get_pair($result_fastq_basename.".fq.gz", $is_paired);
    if (!-s "$result_files[0]") {
        system_call("$script $taxid $res_file $fastq_files->[0] | gzip -c > $result_files[0]");
        system_call("$script $taxid $res_file $fastq_files->[1] | gzip -c > $result_files[1]") if $is_paired;
    }

    Utils::print_and_log_msg("  $result_files[0] $checkmark");
    return (@result_files);
}


