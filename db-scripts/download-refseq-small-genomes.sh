#!/bin/bash

#set -xeu
set -x

function validate_url(){
  if [[ `wget -S --spider $1  2>&1 | egrep 'HTTP/1.1 200 OK|File .* exists.'` ]]; then echo "true"; fi
}
export -f validate_url

function c_echo() {
        printf "\033[33m$*\033[0m\n"
}

ALL="plasmid plastid mitochondrion"

DOMAINS=${1:-$ALL}

echo "Downloading genomes for $DOMAINS"
wget --quiet --no-remove-listing ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/

for DOMAIN in $DOMAINS; do
    if [[ `grep " $DOMAIN" .listing` ]]; then
        c_echo "Downloading for $DOMAIN"
    else
        c_echo "$DOMAIN is not a valid domain - use one of $ALL, or check ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq for valid domains."
        exit 1
    fi

    wget --directory-prefix=_staging/$DOMAIN -nc ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/$DOMAIN/$DOMAIN.*.fna.gz
    gunzip _staging/$DOMAIN/*.gz

    ## split files into one file per sequence
    for i in _staging/$DOMAIN/*.fna; do 
        echo $i && BN=${i/.fna/} && mkdir -p $BN && csplit -s --prefix=$BN/$BN --suffix-format=%04d.fna -z $i '/^>/' '{*}' && rm $i 
    done
done


