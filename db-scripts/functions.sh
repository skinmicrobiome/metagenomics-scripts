function check_or_mkdir {
    echo -n "Checking $1 ..."
    if [[ -d $1 && ! -n `find $1 -prune -empty -type d` ]]; then
        echo "Directory exists - skipping it!"
        #return `false`
        return `true`
    else 
        echo "Downloading files."
        mkdir -p $1
        return `true`
    fi
}


progressfilt () {
    # from http://stackoverflow.com/a/4687912/299878
    local flag=false c count cr=$'\r' nl=$'\n'
    while IFS='' read -d '' -rn 1 c
    do
        if $flag
        then
            printf '%c' "$c"
        else
            if [[ $c != $cr && $c != $nl ]]
            then
                count=0
            else
                ((count++))
                if ((count > 1))
                then
                    flag=true
                fi
            fi
        fi
    done
}
