#!/bin/bash

set -eu

## Functions
function validate_url(){
  if [[ `wget --reject="index.html*" -S --spider $1  2>&1 | egrep 'HTTP/1.1 200 OK|File .* exists.'` ]]; then echo "true"; fi
}
export -f validate_url

function c_echo() {
        printf "\033[34m$*\033[0m\n"
}

function download_n_process() {
    IFS=$'\t' read -r TAXID FILEPATH <<< "$1"

    set -eu
    NAME=`basename "$FILEPATH" .gz`
    #echo "AAAA $BASEDIR/$DOMAIN/$NAME"
    wget --reject="*index.html*" -nc -qO- $FILEPATH | gunzip -c > $BASEDIR/$DOMAIN/$NAME
    if [[ "$CHANGE_HEADER" == "1" ]]; then
        sed -i "s/^>/>kraken:taxid|$TAXID /" $BASEDIR/$DOMAIN/$NAME
    else 
        sed -n "s/^>\([^ ]\+\)\( .*\)\?/\1 $TAXID/p" $BASEDIR/$DOMAIN/$NAME > "$BASEDIR/$DOMAIN.map"
    fi

    if [[ "$DO_DUST" == "1" ]]; then
      dustmasker -infmt fasta -in $BASEDIR/$DOMAIN/$NAME -level 20 -outfmt fasta | sed '/^>/! s/[agctn]/N/g' > $BASEDIR/$DOMAIN/${NAME/.fna/_dustmasked.fna}
      rm $BASEDIR/$DOMAIN/$NAME
    fi

}
export -f download_n_process

#########################################################

ALL_GENOMES="bacteria viral archaea fungi protozoa invertebrate plant vertebrate_mammalian vertebrate_other"
ALL_DATABASES="refseq genbank"
ALL_ASSEMBLY_LEVELS="Complete\ Genome Chromosome Scaffold Contig"

## Option parsing
# TODO check options 
DATABASE="refseq"
ASSEMBLY_LEVEL="Complete Genome"
BASE_DIR="."

N_PROC=1
CHANGE_HEADER=0
DOWNLOAD_RNA=0
DO_DUST=0


USAGE="`basename $0` OPTIONS DOMAINS

Options:
  -h    Display this help message

  -g DATABASE    Database, one of $ALL_DATABASES [$DATABASE]
  -a LEVEL       Assembly level, one of $ALL_ASSEMBLY_LEVELS [$ASSEMBLY_LEVEL]
  -o ORGANISM    Only download specified organism (instead of full domain).
  -l DIRECTORY   Base directory for downloads [$BASE_DIR]
  -j N_PROC      Number of threads for downloading [$N_PROC]
  -r             Download RNA files
  -d             Run dustmasker on downloaded sequences
  -k             Modify header to include taxonomy ID for Kraken

Domains:
  One or more of $ALL_GENOMES.
"

# arguments: $OPTFIND (current index), $OPTARG (argument for option), $OPTERR (bash-specific)
while getopts "g:a:l:j:rdkh" OPT "$@"; do
    case $OPT in
        g) DATABASE=$OPTARG ;;
        a) ASSEMBLY_LEVEL="$OPTARG" ;;
        l) BASE_DIR="$OPTARG" ;;
        j) N_PROC=$OPTARG ;;
        r) DOWNLOAD_RNA=1 ;;
        d) DO_DUST=1 ;;
        k) CHANGE_HEADER=1 ;;
        h) echo "$USAGE" >&2
           exit 1
        ;;
        \?) echo "Invalid option: -$OPTARG" >&2
            echo "$USAGE" >&2
            exit 1 
        ;;
        :) echo "Option -$OPTARG requires an argument." >&2
           exit 1
        ;;
    esac
done
shift $((OPTIND-1))

if [[ "$*" == "" ]]; then
    echo "Specify at least one of the domains $ALL_GENOMES." >&2
    echo "Call `basename $0` -h for help" >&2
    exit 1
fi

DOMAINS=$*

export BASEDIR="$BASE_DIR"
export DO_DUST="$DO_DUST"
export CHANGE_HEADER="$CHANGE_HEADER"

## Fields in the assembly_summary.txt file
TAXID_FIELD=6
SPECIES_TAXID_FIELD=7
VERSION_STATUS_FIELD=11
ASSEMBLY_LEVEL_FIELD=12
FTP_PATH_FIELD=20

echo "Downloading genomes for $DOMAINS at assembly level $ASSEMBLY_LEVEL"
wget -qO- --no-remove-listing ftp://ftp.ncbi.nlm.nih.gov/genomes/$DATABASE/ > /dev/null

for DOMAIN in $DOMAINS; do
    if [[ `grep " $DOMAIN" .listing` ]]; then
        c_echo "Downloading $DOMAIN genomes"
    else
        c_echo "$DOMAIN is not a valid domain - use one of the following:"
        grep '^d' .listing  | sed 's/.* //'
        exit 1
    fi
    export DOMAIN=$DOMAIN
    mkdir -p $BASEDIR/$DOMAIN

    ASSEMBLY_SUMMARY_FILE_FULL="$BASEDIR/$DOMAIN/assembly_summary_full.txt"
    ASSEMBLY_SUMMARY_FILE="$BASEDIR/$DOMAIN/assembly_summary.txt"

    # Download the /$DATABASE/$DOMAIN/assembly_summary.txt file and filter for domain and assembly level
    [[ -f "$ASSEMBLY_SUMMARY_FILE_FULL" ]] || \
    wget -qO- -nc ftp://ftp.ncbi.nlm.nih.gov/genomes/$DATABASE/$DOMAIN/assembly_summary.txt |\
        awk -F "\t" "\$$VERSION_STATUS_FIELD==\"latest\"" > "$ASSEMBLY_SUMMARY_FILE_FULL"

    [[ "$ASSEMBLY_LEVEL" != "Any" ]] &&
        awk -F "\t" "\$$ASSEMBLY_LEVEL_FIELD==\"$ASSEMBLY_LEVEL\"" "$ASSEMBLY_SUMMARY_FILE_FULL" > "$ASSEMBLY_SUMMARY_FILE"

    # Download all the genomic.fna.gz files
    cut -f "$TAXID_FIELD,$FTP_PATH_FIELD,21" "$ASSEMBLY_SUMMARY_FILE" | sed 's/na\t//;s/\t$//' | sed 's#\([^/]*\)$#\1/\1_genomic.fna.gz#' |\
        parallel --bar --gnu -j $N_PROC "download_n_process {}"
    N_EXPECTED=`wc -l "$ASSEMBLY_SUMMARY_FILE"`
    N_GENOMIC=`find $BASEDIR/$DOMAIN -maxdepth 1 -type f -name '*_genomic.fna.gz' | wc -l`
    c_echo "$DOMAIN: expected $N_EXPECTED files, downloaded $N_GENOMIC"

    # Download all the rna.fna.gz files
    if [[ "$DOWNLOAD_RNA" == "1" && ! `echo $DOMAIN | egrep 'bacteria|viral|archaea'` ]]; then
        cut -f $TAXID_FIELD,$FTP_PATH_FIELD  "$ASSEMBLY_SUMMARY_FILE"| sed 's#\([^/]*\)$#\1/\1_rna.fna.gz#' |\
            parallel --bar --gnu -j $N_PROC "[[ \`validate_url {}\` ]] && download_n_process {}"
        N_RNA=`find $BASEDIR/$DOMAIN -maxdepth 1 -type f -name '*_rna.fna.gz' | wc -l`
        c_echo "$DOMAIN: further downloaded $N_RNA RNAs"
    fi
done


