#!/usr/bin/perl
use strict; 
use warnings; 
use Bio::DB::GenBank;
use Bio::SeqIO; 

## Download sequences from NCBI based on the Genbank ID, which is given to STDIN or ARGV.
## Saves each sequence to a separate file name GB_ID.fna

my $gb = Bio::DB::GenBank->new(-retrievaltype => 'tempfile' , -format => 'Fasta'); 
my $outfileformat = 'Fasta'; 

## get GB Ids from STDIN/ARGV

my $cnt = 0;
$|++;
while (<>) {
    printf "\r\r\r\r\r\r%6s", scalar(++$cnt); 
    chomp;
    download_gbs($_);
}

sub download_gbs {
    my ($gb_id) = @_;
    my $seq = $gb->get_Seq_by_version($gb_id);
    my $taxid = $seq->{'species'}->ncbi_taxid;

    my $seq_out = Bio::SeqIO->new('-file' => ">$gb_id.fna",'-format' => $outfileformat); # write each entry in the input file to the output file 
    $seq_out->write_seq($seq->primary_seq); 
}
