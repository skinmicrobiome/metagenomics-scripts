#!/bin/bash
set -eu

## Script for generating a new kraken database

## Output error message in red and exit
display_msg() { echo -e "\e[31m$@\e[0m"; }


DIR=${1:?Usage: `basename "$0"` KRAKEN_DB}

SCRIPT_DIR=`dirname $0`
source $SCRIPT_DIR/functions.sh

[[ -d $DIR ]] && display_msg "Database directory $DIR exists already. Continuing download." || mkdir -vp $DIR
cd $DIR 

# function to display commands
exe() { echo -e "\$ \e[32m$@\e[0m" ; "$@" ; }

# test if flag exists, or 
flagged_exe() {
  NAME=$1
  shift
  if [[ -f "$NAME.flag" ]]; then
    echo "Skipping $NAME, $NAME.flag exists."
  else
    echo -e "$NAME \$ \e[32m$@\e[0m" 
    start=`date +%s`
    "$@"
    end=`date +%s`
    runtime=$( echo "$end - $start" | bc -l )
    echo -e "\e[32mfinished after $runtime seconds\e[0m"
    touch "$NAME.flag"
  fi
}

## download NCBI taxonomy files to taxonomy/

flagged_exe 1_TAXONOMY $SCRIPT_DIR/download-ncbi-taxonomy

## download NCBI genomes for bacteria, viruses, plasmids, and selected refseq genomes to _staging/
flagged_exe 2a_BACTERIAL_N_VIRAL_GENOMES $SCRIPT_DIR/download-refseq-genomes.sh -d -l library -j 10 -k bacteria viral archaea
flagged_exe 2b_PROTOZOA_N_FUNGAL_GENOMES $SCRIPT_DIR/download-refseq-protozoa_n_fungi -fp
flagged_exe 2c_FURTHER_GENOMES $SCRIPT_DIR/download-further-ncbi-genomes _staging

## download all strain viral genomes
flagged_exe 2d_VIRAL_GENOMES $SCRIPT_DIR/download-viral-genomes
## back-up of the taxonomy files
cp taxonomy/nodes.dmp taxonomy/nodes.dmp.sav && cp taxonomy/names.dmp taxonomy/names.dmp.sav
## add new taxonomy IDs and connections to names.dmp and nodes.dmp
flagged_exe 2e_UPDATED_VIRAL_TAXONOMY perl $SCRIPT_DIR/add-taxids-for-neighbors.pl 2 library/viral_genome_acs.nbr taxonomy/names.dmp taxonomy/nodes.dmp library/viral-neighbors
flagged_exe 2e_UPDATED_VIRAL_TAXONOMY perl $SCRIPT_DIR/add-taxids-for-neighbors.pl 2 library/viroid_genome_acs.nbr taxonomy/names.dmp taxonomy/nodes.dmp library/viroid-neighbors

## dust the microbial genomes, and save dusted versions to library/
flagged_exe 3_DUST $SCRIPT_DIR/dust-microbe-genomes

## get sequences in contaminants folder, as well as the UniVec and EmVec databases to library/Contaminants
flagged_exe 4_CONTAMINANTS $SCRIPT_DIR/get-contaminant-seq

## download the human reference genome to library/Human
flagged_exe 5_HUMAN_GENOME $SCRIPT_DIR/download-human-reference-genome
flagged_exe 6_MMUS_GENOME $SCRIPT_DIR/download-mmus-reference-genome

## Add jellyfish1 path
#export PATH=~/src/jellyfish-1.1.11/bin:$PATH
## (For kraken)
flagged_exe 6_BUILD_KRAKENDB kraken-build --db . --threads=4 --build
# and then update the lcas for the Contaminants, such that those are favored
flagged_exe 7_SET_LCA_FOR_CONTAMINANTS ~/src/kraken/install/set_lcas -d database.kdb -i database.idx -n taxonomy/nodes.dmp -m seqid2taxid.map -x -F <( cat library/Contaminants/* )

