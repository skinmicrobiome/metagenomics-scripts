#!/bin/env perl

use strict;
use warnings;
use FindBin '$Bin';
use File::Basename;
use File::Path 'make_path';
use Parallel::ForkManager;

my $VIRUS_TAXID=10239;
my $VIROID_TAXID=12884;


[[ scalar(@ARGV) == 4 ]] || die "four arguments!";

my $DIR = shift;
my $V = shift;
my $MAX_PROCESSES = shift;
## V must be one of viral / viroid

##
my %name_taxid_map;
while (<>) {
    chomp;
    my ($taxid,$name,undef,$sci_name) = split /\t\|\t/;
    #print STDERR "name_taxid_map{$name} = $taxid\n";
    $name_taxid_map{$name} = $taxid;   
}


my $pm = new Parallel::ForkManager($MAX_PROCESSES);

my $nbr_file = "$DIR/${V}_genome_acs.nbr";
print STDERR "Downloading genome ACs into $nbr_file";
if ($V eq "viral") {
    -f "$DIR/viral_genome_acs.nbr" or system("curl -s 'https://www.ncbi.nlm.nih.gov/genomes/GenomesGroup.cgi?taxid=${VIRUS_TAXID}&cmd=download2' -o $nbr_file");
} elsif ($V eq "viroid") {
    -f "$DIR/viroid_genome_acs.nbr" or system("curl -s 'https://www.ncbi.nlm.nih.gov/genomes/GenomesGroup.cgi?taxid=${VIROID_TAXID}&cmd=download2' -o $nbr_file");
} else {
    print STDERR "V must be viral or viroid!!";
    exit 1;
}


#for V in viral viroid; do
#my $FINALDIR = "$DIR/library/$V-strains";
my $FINALDIR = "$DIR/seqs";
make_path($FINALDIR);

open (my $NBR, "<", $nbr_file) or die "blu";
while (<$NBR>) {
    next if /^#/;
    my ($rep_acs, $nbr_ac, undef, $tax) = split /\t/;
    $tax =~ s/.*,//;

    die "No rep taxid for $nbr_ac!!\n" unless defined $name_taxid_map{$tax};
    my $taxid = $name_taxid_map{$tax};
    my $pid = $pm->start and next; 

    my $fna = "$FINALDIR/${nbr_ac}_dustmasked.fna";
    -f $fna or system_call("curl -s 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&id=$nbr_ac&rettype=fasta'".
                           " | dustmasker -infmt fasta -level 20 -outfmt fasta | sed '/^>/! s/[^AGCT]/N/g' > $fna");

    system_call("grep -o '^>[^ ]*' $fna | sed -e 's/.//' -e 's/\$/\t$taxid/'");

    $pm->finish; # Terminates the child process
}

$pm->wait_all_children;

sub system_call {
    system(@_);
}
