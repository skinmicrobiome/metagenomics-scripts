#!/bin/env perl

## add new taxids for virus strains

use strict;
use warnings;
use File::Basename;
use Data::Dumper;

scalar(@ARGV) == 5 || die "USAGE: ".basename($0)." taxid_start_digit genome_acs.nbr names.dmp nodes.dmp neighbor_files_dir\n";

my ($taxid_start_digit,$genome_acs_file,$names_file,$nodes_file,$neighbor_files_dir) = @ARGV;
my $new_neighbor_taxids_start = $taxid_start_digit * 100000000;
#my $new_rep_taxids_start = 100000000;

$|++;

print STDERR "Reading $names_file ... ";
my $max_taxid = 0;
my %name_to_taxid;
open (my $N, "<", $names_file)  or die "$?";
while (<$N>) {
    chomp;
    s/\t\|//g;
    my ($taxid,$name,undef,$level) = split(/\t/);
    $max_taxid = $taxid if $taxid > $max_taxid;
    $name_to_taxid{lc($name)} = $taxid;
}
close($N);
print "DONE\n";

die "$names_file contains taxids > $new_neighbor_taxids_start" if ($max_taxid > $new_neighbor_taxids_start);

#my %rep_neighbors;
my $taxid_for_neighbor = $new_neighbor_taxids_start;
my %processed_reps;
my $i=0;

print STDERR "Reading neighbor files dir ... ";
# read all files from neighbor-files-dir
sub get_ac {
    my ($string) = @_;
    $string =~ s/\..*//;
    return $string;
}
opendir(my $DH, $neighbor_files_dir);
my %neighbor_files = map { get_ac($_) => "$neighbor_files_dir/$_" } readdir($DH);
closedir($DH);
print "DONE\n";

print STDERR "Processing $genome_acs_file ... ";
open (my $NAMES,">>",$names_file) or die "Couldn't open $names_file for writing";
open (my $NODES,">>",$nodes_file) or die "Couldn't open $nodes_file for writing";

my %rep_taxid;
open (my $V,"<",$genome_acs_file) or die;
<$V>; <$V>;  ## skip header lines
while (my ($representative,$neighbor_ac,$host,$lineage,$taxonomy_name,$segment) = split(/\t/,<$V>)) {
    ## Map taxonomy name to taxid
    my @lineage = split(/,/,$lineage);
    my $rep_taxid = $name_to_taxid{lc($lineage[$#lineage])};
    defined $rep_taxid or die "representative taxid not defined for $taxonomy_name!!\n";
    
    ## Add new taxid for neighbor to names.dmp 
    ##  (max so far is 1,765,964 - let's take 10,000,000+X for new ones)
    print $NAMES dmp_fmt(++$taxid_for_neighbor,"$taxonomy_name neighbor $neighbor_ac","","scientific name");
    print $NODES dmp_fmt($taxid_for_neighbor,$rep_taxid,"strain","",0,1,11,1,0,1,0,0);
    
    ## Update headers of viral genome files
    if (defined $neighbor_files{$neighbor_ac}) {
        `sed -i 's/^>/>kraken:taxid|$taxid_for_neighbor /' $neighbor_files{$neighbor_ac}`;
    } else {
        print STDERR "\nWarning: No sequence file for $neighbor_ac";
    }

    ++$i;
}
close($V);
close($NAMES);
close($NODES);
print "DONE\n";

print STDERR "Added $i lines to $names_file and $nodes_file!\n";

sub dmp_fmt {
    return(join("\t|\t",@_)."\t|\n");
}
