
The directory structure of the download files for Centrifuge is the same as Kraken. That means that you can build a Centrifuge database based on a pre-existing Kraken library directory structure, or a Kraken database based on the files downloaded and processed by Centrifuge scripts.

There are three directories created by the scripts:

taxonomy/       contains GI to taxonomy ID mapping, and the hierachical taxonomy tree
library/        this folder contains the kinds of sequences
                  - microbial genomes with low-complexity regions masked
                  - host genomes, typically human
                  - contaminating sequences (vectors, adaptors, primers)
_staging/       microbial genomes are first downloaded into the staging folder where they await dust-masking

Allowed file extensions are {ffn,fna,fa,fasta}. It is strongly recommended to execute these scripts in an empty folder.

SCRIPTS=/path/to/centifuge/db-scripts
mkdir /path/to/refdb/dir
cd /path/to/refdb/dir

## download NCBI taxonomy files to taxonomy/
$SCRIPTS/download-ncbi-taxonomy

## download NCBI genomes for bacteria, viruses, plasmids, and selected refseq genomes to _staging/
$SCRIPTS/download-microbial-reference-genomes
$SCRIPTS/download-refseq-protozoa_n_fungi-genomes
$SCRIPTS/download-further-ncbi-genomes

## dust the microbial genomes, and save dusted versions to library/
$SCRIPTS/dust-microbe-genomes

## get sequences in contaminants folder, as well as the UniVec and EmVec databases to library/Contaminants
$SCRIPTS/get-contaminant-seq

## download the human reference genome to library/Human
$SCRIPTS/download-human-reference-genome

## (For kraken)
# run a first kraken-build
# and then update the lcas for the Contaminants, such that those are favored
set_lcas -d database.kdb -i database.idx -n taxonomy/nodes.dmp -m seqid2taxid.map -x -F <( cat library/Contaminants/* )
