#!/usr/bin/perl
use strict; 
use warnings;
use Getopt::Std;
use Bio::DB::GenBank;
use Bio::SeqIO; 

our $opt_d = ".";
getopts('d:');

## Download sequences from NCBI based on the Genbank ID, which is given to STDIN or ARGV.
## Saves each sequence to a separate file name GB_ID.fna

my $gb = Bio::DB::GenBank->new(-retrievaltype => 'tempfile' , -format => 'Fasta'); 
my $gbs_at_a_time = 100;    ## number of entries that are retrieved at a time from GenBank. Too large numbers cause errors
my $outfileformat = 'Fasta'; 

## get GB Ids from STDIN/ARGV
my @gbs = <>;
chomp @gbs;

print "Getting ".scalar(@gbs)." entries from Genbank.\n";
$|++;

while (my @some_gbs = splice(@gbs,0,$gbs_at_a_time)) {
    printf "\r\r\r\r\r\r\r\r\r\r\r%6s left", scalar(@gbs); 
    download_gbs(@some_gbs);
}

sub download_gbs {
    my @gbs = @_;
    my $seq_in = $gb->get_Stream_by_acc(\@gbs); 
    while (my $seq = $seq_in->next_seq) {
        my $id = $seq->primary_id;
        my ($gi_name,$gi,$gb_name,$gb) = split(/\|/,$id);
        $gi_name eq 'gi' or die "$id?\n";
        defined $gb_name or die "$id?\n";

        my $seq_out = Bio::SeqIO->new('-file' => ">$opt_d/$gb.fna",'-format' => $outfileformat); # write each entry in the input file to the output file 
        $seq_out->write_seq($seq); 
    }
}
