#!/bin/bash

# This script converts Kraken's tabular output to a standard taxonomic format usable by tools such as Krona
#      by: Thomas Mehoke
# written: November 7, 2014

runtime=$(date +"%Y%m%d%H%M%S%N")

usage()
{
cat << EOF
usage: $0 -i <input.kraken> -k </path/to/kraken-db>


OPTIONS:
   -h      show this message
   -i      output file from Kraken
   -k      Kraken database folder (path)
   -u      ignore unclassified results (taxid = 0)
   -o      file to place text output file
   -r      file to place Krona HTML file
   -p      path to Krona install directory (default: /data/apps/src/KronaTools-2.4)
   -d      threads to use for parallel processing
            The default is one less than the available processors.
            On this computer ($(hostname)) there are $(nproc) threads,
             so the default is $(($(nproc) - 1)).
   -w      working directory (default: /tmp)
EOF
}

# set default values
ignore="false"
workdir="/tmp"
THREADS=$(($(nproc) - 1))
kronafile=""
KRONA_PATH="/data/apps/src/KronaTools-2.4"
outputfile=""

# parse input arguments
while getopts "hi:k:uw:d:o:r:p:" OPTION
do
	case $OPTION in
		h) usage; exit 1 ;;
		i) input=$OPTARG ;;
		k) DB=$OPTARG ;;
		u) ignore="true" ;;
		w) workdir=$OPTARG ;;
		d) THREADS=$OPTARG ;;
		o) outputfile=$OPTARG ;;
		r) kronafile=$OPTARG ;;
		p) KRONA_PATH=$OPTARG ;;
		?) usage; exit ;;
	esac
done

# if necessary arguments are not present, display usage info and exit
if [[ -z "$input" ]]; then
	echo "Specify a Kraken output file with -i" >&2
	usage
	exit 2
fi
if [[ -z "$DB" ]]; then
	echo "Select a kraken database with -k" >&2
	usage
	exit 2
fi
kronascript="$KRONA_PATH/src/krona-2.0.js"
if ! [[ -s "$kronascript" ]]; then
	echo "Error: $KRONA_PATH/src/krona-2.0.js does not exist"
	usage
	exit 2
fi

# Need to add format checking to make sure kraken output is properly formatted
# Need to add check that Kraken DB contains names.dmp and nodes.dmp

#===================================================================================================
# function declarations

make_tax_string() {

	# parse input arguments
	count="$1"
	tax="$2"

	# pull time so temp files don't overwrite each other
	runtime_sub=$(date +"%Y%m%d%H%M%S%N")

	# pull out name and parent taxid for this taxon
	name=$(egrep "^$tax"$'\t' "$DB/taxonomy/names.dmp" | grep "scientific name" | cut -f3)
	parent=$(egrep "^$tax"$'\t' "$DB/taxonomy/nodes.dmp" | cut -f3)

	# deal with taxa near the root
	if [[ $tax -eq 0 ]]; then
		echo -e "$count\tUnclassified" >> "$OUTPUT"
	elif [[ $parent -eq 1 ]]; then
		if [[ $ignore == "true" ]]; then
			echo -e "$count\t$name" >> "$OUTPUT"
		elif [[ "$name" != "root" ]]; then
			echo -e "$count\troot\t$name" >> "$OUTPUT"
		else
			echo -e "$count\troot" >> "$OUTPUT"
		fi
	# for all others, create full taxonomic string
	else
		while [[ $parent -gt 1 ]]; do
			parentname=$(egrep "^$parent"$'\t' "$DB/taxonomy/names.dmp" | grep "scientific name" | cut -f3)
			name="$parentname\t$name"
			echo -e "$name" > "$tempdir/krona.temp-$runtime_sub"
			parent=$(egrep "^$parent"$'\t' "$DB/taxonomy/nodes.dmp" | cut -f3)
		done
		if [[ $ignore == "true" ]]; then
			echo -e "$count\t$(cat "$tempdir/krona.temp-$runtime_sub")" >> "$OUTPUT"
		else
			echo -e "$count\troot\t$(cat "$tempdir/krona.temp-$runtime_sub")" >> "$OUTPUT"
		fi
	fi
}

#===================================================================================================

tempdir="$workdir/kraken2krona-$runtime"
mkdir -p "$tempdir"
tax_counts="$tempdir/tax_counts"
OUTPUT="$tempdir/output"

# create space-separated count of taxonomic ranks
if [[ $ignore == "true" ]]; then
	cut -f3 "$input" | sort -T "$tempdir" | uniq -c | sed 's/^ *//' | sort -T "$tempdir" -k1nr,1 | awk -F" " '{if($2 > 1){print $0}}' > "$tax_counts"
else
	cut -f3 "$input" | sort -T "$tempdir" | uniq -c | sed 's/^ *//' | sort -T "$tempdir" -k1nr,1 > "$tax_counts"
fi

# loop through all taxonomic levels
export DB
export tempdir
export ignore
export OUTPUT
export -f make_tax_string
parallel -a "$tax_counts" -d $'\n' --colsep ' ' -n 1 -P $THREADS make_tax_string {1} {2}
#cat "$tax_counts" | while read count tax; do
#	make_tax_string $tax $count >> "$OUTPUT"
#done

# generate krona plot if specified by -k flag
if [[ -n "$kronafile" ]]; then
	export TERM=xterm-256color
	ktImportText -o "$kronafile" -n $(basename "$DB") "$OUTPUT"

	# add javascript directly to the HTML file so it can be read anywhere without an internet connection
	sed -i '/<script id="notfound">.*/d' "$kronafile"
	sed -i 's@<script src="http://krona.sourceforge.net/src/krona-2.0.js"></script>@<script>\n  </script>@' "$kronafile"
	sed -i "/<script>/ r $kronascript" "$kronafile"

	# make file viewable without an internet connection (I'm not sure why this line causes errors, but commenting it seems harmless)
	sed -i "s/^\(\s*hiddenPattern = context.createPattern(image, 'repeat');\)/\/\/\1/" "$kronafile"

	# unselect the collapse box
	sed -i "s/\(collapse = kronaElement.getAttribute('collapse') == '\)true';/\1false';/" "$kronafile"
fi

# output to STDOUT
if [[ -z "$outputfile" ]]; then
	sort -T "$tempdir" -k1nr,1 "$OUTPUT"
# or output to output file specified by -o argument
else
	sort -T "$tempdir" -k1nr,1 "$OUTPUT" > "$outputfile"
fi

rm -rf "$tempdir"
#~~eof~~#
