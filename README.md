# Collection of scripts for metagenomics analysis

## Database building scripts

The main script for genome download and database creation is `db-scripts/create-new-krakendb-sh <database location>`. This script does

 - Download all complete archaeal, bacterial, and viral genomes from Refseq (see http://www.ncbi.nlm.nih.gov/genome/doc/ftpfaq/)
    - As of Jan 14, 2016:
         - 202 archaea genomes
         - 4111 bacteria genomes
         - 5412 viral genomes
    - RefSeq only has one strain per viral species, others are listed as neig hbors
 - Download all viral strains ('neighbors') from NCBI Viral genome resource (http://www.ncbi.nlm.nih.gov/genomes/GenomesGroup.cgi?taxid=10239&cmd=download2)
     - Added 84272 viral strain genomes
 - Modify the taxonomy to include specific taxonomy ID for each viral strain
     - Newly added taxonomy IDs are above 200,000,000 - actual taxonomy IDs are below 100,000
     - Names are 'VIRUS_NAME neighbor ACCESSION' - e.g. 
 - Download specific fungal and protist genomes
     - Fungal genomes: Aspergillus fumigatus,Candida albicans,Candida dubliniensis,Candida glabrata,Coccidioides immitis,Coccidioides posadasii ASM15005v1,Cryptococcus gattii,Cryptococcus neoformans,Encephalitozoon cuniculi,Encephalitozoon intestinalis,Histoplasma capsulatum ASM15100v2,Paracoccidioides brasiliensis,Rhizopus delemar RO3,Saccharomyces cerevisiae R64  
      - Protist genomes: Babesia microti strain RI,Cryptosporidium parvum,Dictyostelium discoideum,Giardia lamblia GL2,Leishmania braziliensis,Leishmania donovani,Leishmania infantum,Leishmania major,Plasmodium falciparum,Toxoplasma gondii TGA4,Trypanosoma brucei
 - Dust all microbial genomes with NCBI dustmasker, level 20
 - Download human genome GRCh
 - Download mouse genome XX
 - Download contaminant sequences
     - self defined, and full UniVec and EmVec databases
 - Build database with kraken
 - Re-set the kmers appearing in contaminants to the contaminant taxonomy ID
     - requires custom version of Kraken from https://github.com/fbreitwieser/kraken